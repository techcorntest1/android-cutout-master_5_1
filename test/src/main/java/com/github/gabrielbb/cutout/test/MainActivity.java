package com.github.gabrielbb.cutout.test;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.gabrielbb.cutout.CutOut;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    public Uri imageIconUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageView = findViewById(R.id.imageView);
        openFile();
//        final Uri imageIconUri = getUriFromDrawable(R.drawable.image_icon);
//        imageView.setImageURI(imageIconUri);
//        imageView.setTag(imageIconUri);

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(view -> {

//            final Uri testImageUri = getUriFromDrawable(R.drawable.test_image);

            CutOut.activity()
                    .src(imageIconUri)
                    .bordered()
                    .noCrop()
                    .start(this);
        });

    }
    private void openFile() {
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        i.setType("image/*");
        startActivityForResult(i, 3000);
    }
    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CutOut.CUTOUT_ACTIVITY_REQUEST_CODE) {

            switch (resultCode) {
                case Activity.RESULT_OK:
                    Uri imageUri = CutOut.getUri(data);
                    // Save the image using the returned Uri here
                    imageView.setImageURI(imageUri);
//                    System.out.print("User cancelled the CutOut screen"+imageUri);
//                    Toast.makeText(this, "Image path " + imageUri, Toast.LENGTH_LONG).show();

                    imageView.setTag(imageUri);
                    savefile(imageUri);
//                    Bitmap bitUri=getContactBitmapFromURI(this,imageUri);
//                    saveBitmapIntoSDCardImage(this,bitUri);
                    break;
                case CutOut.CUTOUT_ACTIVITY_RESULT_ERROR_CODE:
                    Exception ex = CutOut.getError(data);
                    break;
                default:
                    System.out.print("User cancelled the CutOut screen");
            }
        }else if(requestCode == 3000){
            if(data!=null){
                if(data.getClipData()!=null){
                    ClipData clipData = data.getClipData();
                    for(int i = 0; i < clipData.getItemCount(); i++)
                    {
                        ClipData.Item path = clipData.getItemAt(i);
                          imageIconUri = path.getUri();
//                        getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);

                        imageView.setImageURI(imageIconUri);
                        imageView.setTag(imageIconUri);
                        //                        stickerPack.addSticker(uri, this);
                    }
                } else {
                     imageIconUri = data.getData();
//                    getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    imageView.setImageURI(imageIconUri);
                    imageView.setTag(imageIconUri);
                    //                    stickerPack.addSticker(uri, this);
                }
//                finish();uri
//                startActivity(getIntent());
            }
        }
    }

    void savefile(Uri sourceuri)
    {
        String sourceFilename= sourceuri.getPath();
        String destinationFilename = android.os.Environment.getExternalStorageDirectory().getPath()+ File.separatorChar+"abc.png";

        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(sourceFilename));
            bos = new BufferedOutputStream(new FileOutputStream(destinationFilename, false));
            byte[] buf = new byte[1024];
            bis.read(buf);
            do {
                bos.write(buf);
            } while(bis.read(buf) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


//    public  Bitmap getContactBitmapFromURI(Context context, Uri uri) {
//        try {
//
//            InputStream input = context.getContentResolver().openInputStream(uri);
//            if (input == null) {
//                return null;
//            }
//            return BitmapFactory.decodeStream(input);
//        }
//        catch (FileNotFoundException e)
//        {
//
//        }
//        return null;
//
//    }
//
//    public  File saveBitmapIntoSDCardImage(Context context, Bitmap finalBitmap) {
//
//        String root = Environment.getExternalStorageDirectory().toString();
//        File myDir = new File(root);
//        myDir.mkdirs();
//
//        String fname = "file_name" + ".png";
//        File file = new File (myDir, fname);
//
//        try {
//            FileOutputStream out = new FileOutputStream(file);
//            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
//            out.flush();
//            out.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return file;
//    }

    public Uri getUriFromDrawable(int drawableId) {
        System.out.print("********************getUriFromDrawable");


        return Uri.parse("android.resource://" + getPackageName() + "/drawable/" + getApplicationContext().getResources().getResourceEntryName(drawableId));
    }
}
