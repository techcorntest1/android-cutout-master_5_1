package com.smartstickercreator;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.app.NavigationPolicy;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;
import static com.smartstickercreator.FilesUtils.YOUTUBE_API_KEY;

public class NewUserIntroActivity extends IntroActivity implements YouTubePlayer.OnInitializedListener {
    public static SharedPreferences spf;
    private static final int RECOVERY_REQUEST = 1;
    YouTubePlayerSupportFragment youTubeView;
    private MyPlayerStateChangeListener playerStateChangeListener;
    private MyPlaybackEventListener playbackEventListener;
     AlertDialog ad;
    @Override protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setButtonBackVisible(false);
//        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//

            addSlide(new SimpleSlide.Builder()
                    .title("Smart WhatsApp Stickers!")
                    .description("Make your own sticker | Share with friends | and lots of fun")
                    .image(R.drawable.logo)
                    .background(R.color.colorAccent)
                    .scrollable(false)
                    .build());
//        addSlide(new SimpleSlide.Builder().title("Allow storage permission for WhatsApp")
//                .background(R.color.colorAccent)
//                .scrollable(false)
//                .buttonCtaLabel("Tap Here")
//                .buttonCtaClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//
////                        int permissionCheck = ContextCompat.checkSelfPermission(NewUserIntroActivity.this, android.permission.com.whatsapp.STORAGE);
//                        Log.e("i","per"+ );
//                    }
//                })
//                .build());
//
//        if(!checkIfBatteryOptimizationIgnored() && Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
//            addSlide(new SimpleSlide.Builder()
//                    .title("We've recognized our app is optimized by Android's Doze system.")
//                    .description("In order for the app to work correctly, please disable the optimization for \"Smart Stickers\" after clicking the button.")
//                    .background(R.color.colorAccent)
//                    .scrollable(false)
//
//                    .buttonCtaLabel("Let's Go")
//                    .buttonCtaClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            AlertDialog.Builder dialog = new AlertDialog.Builder( NewUserIntroActivity.this);
//                            LayoutInflater layoutInflater = getLayoutInflater();
////                            LayoutInflater.from(NewUserIntroActivity.this)
//                            View promptView = layoutInflater.inflate(R.layout.video_layout, null);
//
//                            youTubeView = (YouTubePlayerSupportFragment)getSupportFragmentManager().findFragmentById(R.id.youtube_view);
//                            youTubeView.initialize(YOUTUBE_API_KEY,NewUserIntroActivity.this);
//                            playerStateChangeListener = new
//
//                                    MyPlayerStateChangeListener();
//
//                            playbackEventListener = new
//
//                                    MyPlaybackEventListener();
//                            Button doseetings = (Button) promptView.findViewById(R.id.doseetings);
//                            doseetings.setOnClickListener(new View.OnClickListener()
//
//                            {
//                                @Override
//                                public void onClick(View v) {
//
//
//                                    Intent intent = new Intent();
//                                    intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
//                                    startActivityForResult(intent, 4113);
//                                }
//                            });
//                            dialog.setView(promptView);
////        dialog.setTitle("Battery Optimization");
////        dialog.setMessage("Please add creator name for the pack of Stickers.");
//                            dialog.setCancelable(false);
//                             ad = dialog.create();
//
//                            ad.show();
//
////                             VideoView v1= new VideoView();
////                             v1.onCreate();
////                            Intent intent=new Intent(NewUserIntroActivity.this,VideoViewActivity.class);
////                            startActivity(intent);
//
//
//
//                        }
//                    })
////                    .buttonCtaLabel("Ask me How?")
////                    .buttonCtaClickListener(new View.OnClickListener() {
////                        @Override
////                        public void onClick(View view) {
////                            setName();
////                        }
////                    })
//                    .build());
//        }
        addSlide(new SimpleSlide.Builder()
                .title("Tell us your name")
//                .description("Please name for the creator.")
                .background(R.color.colorAccent)
                .scrollable(false)

                .buttonCtaLabel("Tap Here")
                .buttonCtaClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setName();

                    }
                })
                .build());



            addSlide(new SimpleSlide.Builder()
                    .title("Lets's Start!")
                    .background(R.color.colorAccent)
                    .scrollable(false)

////                    .buttonCtaLabel("Tap here and tell me your name")
//                    .buttonCtaClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
////                            verifyStoragePermissions(NewUserIntroActivity.this);
//                            setName();
//                        }
//                    })
                    .build());



//        addSlide(new SimpleSlide.Builder()
//                .title("Let's Start!")
////                .description("You can start creating your sticker packs!\n\n" +
////                        "Please notice! Sticker packs created or loaded with the app require the app being installed on device.\n\n" +
////                        "Uninstalling the app will cause of removing the sticker packs from your WhatsApp client " +
////                        "and will be lost if they weren't shared.")
//                .background(R.color.colorAccent)
//                .scrollable(false)
//                .build()
//
//
//        );

        addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if(i>0 && i!=getSlides().size()-1){
                    setNavigation(false, true);
                } else {
                    setNavigation(true, false);

                }


            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


    }

    private  final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

        @Override
        public void onPlaying() {
            // Called when playback starts, either due to user action or call to play().
//            showMessage("Playing");
        }

        @Override
        public void onPaused() {
            // Called when playback is paused, either due to user action or call to pause().
//            showMessage("Paused");
        }

        @Override
        public void onStopped() {
            // Called when playback stops for a reason other than being paused.
//            showMessage("Stopped");
        }

        @Override
        public void onBuffering(boolean b) {
            // Called when buffering starts or ends.
        }

        @Override
        public void onSeekTo(int i) {
            // Called when a jump in playback position occurs, either
            // due to user scrubbing or call to seekRelativeMillis() or seekToMillis()
        }
    }

    private  final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        @Override
        public void onLoading() {
            // Called when the player is loading a video
            // At this point, it's not ready to accept commands affecting playback such as play() or pause()
        }

        @Override
        public void onLoaded(String s) {
            // Called when a video is done loading.
            // Playback methods such as play(), pause() or seekToMillis(int) may be called after this callback.
        }

        @Override
        public void onAdStarted() {
            // Called when playback of an advertisement starts.
        }

        @Override
        public void onVideoStarted() {
            // Called when playback of the video starts.
        }

        @Override
        public void onVideoEnded() {
            // Called when the video reaches its end.
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
            // Called when an error occurs.
        }
    }

    public void setName(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Name of Creator for Sticker Collection");
//        dialog.setMessage("Please add creator name for the pack of Stickers.");
        dialog.setCancelable(false);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);


        LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonLayoutParams.setMargins(50, 0, 50, 10);


        final EditText creatorBox = new EditText(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            creatorBox.setAutofillHints("name");
        }
        creatorBox.setLines(1);
        creatorBox.setLayoutParams(buttonLayoutParams);
        creatorBox.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);
        creatorBox.setHint("Creator Name");
        layout.addView(creatorBox);

        dialog.setView(layout);

        dialog.setPositiveButton("SEND", null);

//        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                ad.show();
////                dialog.cancel();
//            }
//        });

        final AlertDialog ad = dialog.create();

        ad.show();

        Button b = ad.getButton(AlertDialog.BUTTON_POSITIVE);
        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view){

                if(TextUtils.isEmpty(creatorBox.getText())){
                    creatorBox.setError("Creator name is required!");
                }

                if( !TextUtils.isEmpty(creatorBox.getText())) {
                    Log.e("FG","h"+creatorBox.getText().toString());
                     spf=getSharedPreferences("init", MODE_PRIVATE);
                    SharedPreferences.Editor editor;
                    editor=spf.edit();
                    editor.putString("creator",creatorBox.getText().toString());
                    editor.apply();
                    ad.dismiss();
                    setNavigation(true, false);
                    nextSlide();
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    SharedPreferences.Editor editor1  = prefs.edit();
                    editor1.putBoolean("isAlreadyShown", false);
                    editor1.commit();
//                    createDialogForPickingIconImage(nameBox, creatorBox);
                }
            }
        });

//        creatorBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    b.performClick();
//                }
//                return false;
//            }
//        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 4113){

            if(checkIfBatteryOptimizationIgnored()){

                setNavigation(true, false);
                ad.dismiss();
                nextSlide();
            }
        }
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(YOUTUBE_API_KEY, this);
        }
    }
public  void doOptimize(){
    if(checkIfBatteryOptimizationIgnored()){

        setNavigation(true, false);

        nextSlide();
    }
}

    private  boolean checkIfBatteryOptimizationIgnored(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            return pm.isIgnoringBatteryOptimizations(packageName);
        } else {
            return true;
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayer.Provider) youTubeView;
    }

    private  void setNavigation(boolean forward, boolean backward){
        setNavigationPolicy(new NavigationPolicy() {
            @Override
            public boolean canGoForward(int i) {
                return forward;
            }

            @Override
            public boolean canGoBackward(int i) {
                return backward;
            }
        });
    }

    public static void verifyStoragePermissions(Activity activity){
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED){
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    1
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    AlertDialog alertDialog = new AlertDialog.Builder(this)
                            .setPositiveButton("Let's Go", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    verifyStoragePermissions(NewUserIntroActivity.this);
                                }
                            })
                            .create();
                    alertDialog.setTitle("Notice!");
                    alertDialog.setMessage("Allowing storage permissions is crucial for the app to work. Please grant the permissions.");
                    alertDialog.show();
                } else {
                    setNavigation(true, false);
                    setName();

                }
                break;
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        moveTaskToBack(true);

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        youTubePlayer.setShowFullscreenButton(false);

        if (!b) {
            youTubePlayer.cueVideo("HAhBsXps61U"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    public class VideoView extends YouTubeBaseActivity {

        public void onCreate() {
            Log.e("on","create");
//            LayoutInflater layoutInflater = LayoutInflater.from(NewUserIntroActivity.this);
//            View promptView = layoutInflater.inflate(R.layout.video_layout, null);
//            AlertDialog.Builder dialog = new AlertDialog.Builder( NewUserIntroActivity.this);
////            youTubeView=(YouTubePlayerView)findViewById(R.id.youtube_view);
////            youTubeView.initialize(YOUTUBE_API_KEY,NewUserIntroActivity.this);
//            youTubeView = (YouTubePlayerSupportFragment)getSupportFragmentManager().findFragmentById(R.id.youtube_view);
//            youTubeView.initialize(YOUTUBE_API_KEY, (YouTubePlayer.OnInitializedListener) this);
//            playerStateChangeListener = new
//
//                    MyPlayerStateChangeListener();
//
//            playbackEventListener = new
//
//                    MyPlaybackEventListener();
//            dialog.setView(promptView);
////        dialog.setTitle("Battery Optimization");
////        dialog.setMessage("Please add creator name for the pack of Stickers.");
//            dialog.setCancelable(true);
//            Button doseetings = (Button) promptView.findViewById(R.id.doseetings);
//            doseetings.setOnClickListener(new View.OnClickListener()
//
//            {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent();
//                    intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
//                    startActivityForResult(intent, 4113);
//                }
//            });
//
//            final AlertDialog ad = dialog.create();
//
//            ad.show();
        }



        private  final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

            @Override
            public void onPlaying() {
                // Called when playback starts, either due to user action or call to play().
//            showMessage("Playing");
            }

            @Override
            public void onPaused() {
                // Called when playback is paused, either due to user action or call to pause().
//            showMessage("Paused");
            }

            @Override
            public void onStopped() {
                // Called when playback stops for a reason other than being paused.
//            showMessage("Stopped");
            }

            @Override
            public void onBuffering(boolean b) {
                // Called when buffering starts or ends.
            }

            @Override
            public void onSeekTo(int i) {
                // Called when a jump in playback position occurs, either
                // due to user scrubbing or call to seekRelativeMillis() or seekToMillis()
            }
        }

        private  final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

            @Override
            public void onLoading() {
                // Called when the player is loading a video
                // At this point, it's not ready to accept commands affecting playback such as play() or pause()
            }

            @Override
            public void onLoaded(String s) {
                // Called when a video is done loading.
                // Playback methods such as play(), pause() or seekToMillis(int) may be called after this callback.
            }

            @Override
            public void onAdStarted() {
                // Called when playback of an advertisement starts.
            }

            @Override
            public void onVideoStarted() {
                // Called when playback of the video starts.
            }

            @Override
            public void onVideoEnded() {
                // Called when the video reaches its end.
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {
                // Called when an error occurs.
            }
        }
    }
}
