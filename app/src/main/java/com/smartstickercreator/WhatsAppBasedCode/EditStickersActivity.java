package com.smartstickercreator.WhatsAppBasedCode;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.smartstickercreator.CutOut;
import com.github.siyamed.shapeimageview.ShapeImageView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.smartstickercreator.BuildConfig;
import com.smartstickercreator.Point;
import com.smartstickercreator.R;
import com.smartstickercreator.StickerBook;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.smartstickercreator.BitmapUtility.getResizedBitmap;
import static com.smartstickercreator.StickerBook.getStickerPackByIdWithContext;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackDetailsActivity.ADD_PACK;
//import static com.smartstickercreator.WhatsAppBasedCode.StickerPackDetailsActivity.prefi;
//import com.github.gabrielbb.cutout.CutOut;

public class EditStickersActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String EXTRA_STICKER_PACK_DATA = "sticker_pack";
    private ImageView imageView2,imageeditback;
    public LinearLayout lier,licir,lielli,lisq,maincontainer;
    public Uri imageIconUri;
//    public View halfalphaColorView;
    public Uri IconUri= Uri.parse("");
//    private View fullColorView, halfalphaColorView, alphaColorView;
    public StickerPack stickerPack;
    String identifier;

    public AlertDialog alertDialog=null;

    String shape="";
    public ImageView resultImageViewCust;

    //  SharedPreferences pref;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_stickers);
        imageView2=(ImageView)findViewById(R.id.imageView);
        imageeditback=(ImageView)findViewById(R.id.imageeditback);

//  final Uri imageIconUri = getUriFromDrawable(R.drawable.logo);
        lier=(LinearLayout)findViewById(R.id.eraser);
        licir=(LinearLayout)findViewById(R.id.circle);
        lielli=(LinearLayout)findViewById(R.id.heart);
        lisq=(LinearLayout)findViewById(R.id.square);
        alertDialog = new AlertDialog.Builder(EditStickersActivity.this).create();

        lier.setOnClickListener(this);
        licir.setOnClickListener(this);
        lielli.setOnClickListener(this);
        lisq.setOnClickListener(this);
//        imageView2.setImageURI(imageIconUri);
//        imageView2.setTag(imageIconUri);
        identifier=getIntent().getStringExtra("stickerPack");
        stickerPack=StickerBook.getStickerPackById(identifier);
        Log.e("stiv","is"+stickerPack);
        openFile();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Edit Photo");

        }


        imageeditback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                Intent intent2 = new Intent(EditStickersActivity.this, StickerPackDetailsActivity.class);
////            intent.putStringArrayListExtra("list",(ArrayList<Sticker>)stickers);
//                intent2.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_DATA, identifier);
////            intent.putStringArrayListExtra("stickers",stickers);
//                startActivity(intent2);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
//        final Uri testImageUri = getUriFromDrawable(R.drawable.test_image);
        fab.setOnClickListener(view -> {
//        if(IconUri.equals(Uri.parse(""))){
    Log.e("**fabURI","**FabURi"+IconUri);
            if(IconUri.equals(Uri.parse("")) ){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please Edit this image first")
                    .setCancelable(true)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id){
                            dialog.dismiss();
//                        openFileTray(nameBox.getText().toString(), creatorBox);
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        }else {
            stickerPack.addSticker(IconUri, this);
//            add to shared pref..
//            pref= getSharedPreferences("stickers", Context.MODE_PRIVATE);

            List<Sticker> stickers = stickerPack.getStickers();
            Log.e("uri", "g" + IconUri + "  stickers" + stickers);
            SharedPreferences.Editor editor;
//            prefi= getSharedPreferences("Stickers", Context.MODE_PRIVATE);
//            editor=prefi.edit();
////        editor.pu
//            Gson gson = new Gson();
//            String json = gson.toJson(stickers);
//            editor.putString("stickers", json);
//            editor.apply();     //
//            editor.commit();
////            stickers
//            Intent intent1 = new Intent(Intent.ACTION_SEND);
//            intent1.setType("text/plain");
//            intent1.setPackage("com.whatsapp");
//            intent1.putExtra(Intent.EXTRA_TEXT, "Smart Stickers");
////            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent1);
//            openWhatsApp();

//            AlertDialog alertDialog = new AlertDialog.Builder(EditStickersActivity.this)
//                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
                            Intent intent = new Intent(EditStickersActivity.this, StickerPackDetailsActivity.class);
                        intent.putExtra("add",true);
                            intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_DATA, identifier);
//            intent.putParcelableArrayListExtra("stickers", (ArrayList<? extends Parcelable>) stickers);
                            startActivity(intent);
//                        }
//                    })
//                    .create();
//            alertDialog.setMessage("Please, Do not close this App just Reopen WhatsApp to see all sticker inside this collection.");
//            alertDialog.show();
//            Toast.makeText(this,"" , Toast.LENGTH_LONG).show();
        }
        });

    }
    private void openWhatsApp() {
        String smsNumber = "91XXXXXXXX20";
        boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
        if (isWhatsappInstalled) {
            Log.e("i","odj");
            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(smsNumber) + "@s.whatsapp.net");//phone number without "+" prefix

            startActivity(sendIntent);
        } else {
            Uri uri = Uri.parse("market://details?id=com.whatsapp");
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            Toast.makeText(this, "WhatsApp not Installed",
                    Toast.LENGTH_SHORT).show();
            startActivity(goToMarket);
        }
    }

    private boolean whatsappInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }


    private int getColorWithAlpha(int color, float ratio) {
        int newColor = 0;
        int alpha = Math.round(Color.alpha(color) * ratio);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        newColor = Color.argb(alpha, r, g, b);
        return newColor;
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR1)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Activity.REQUEST","Activity.REQUEST"+requestCode);

        LayoutInflater layoutInflater = LayoutInflater.from(EditStickersActivity.this);
        View promptView = layoutInflater.inflate(R.layout.edit_photo_modal, null);

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        alertDialog= adb.setView(new View(this)).create();
        // (That new View is just there to have something inside the dialog that can grow big enough to cover the whole screen.)

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        alertDialog.getWindow().setAttributes(lp);

//        alertDialog.getWindow().setLayout(
//                        WindowManager.LayoutParams.MATCH_PARENT,
//                        WindowManager.LayoutParams.MATCH_PARENT
//                );
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        lier=(LinearLayout)promptView.findViewById(R.id.eraser);
        licir=(LinearLayout)promptView.findViewById(R.id.circle);
        lielli=(LinearLayout)promptView.findViewById(R.id.heart);
        lisq=(LinearLayout)promptView.findViewById(R.id.square);

        lier.setOnClickListener(this);
        licir.setOnClickListener(this);
        lielli.setOnClickListener(this);
        lisq.setOnClickListener(this);
//        alertDialog.setMessage("Please, Do not close this App just Reopen WhatsApp to see all sticker inside this collection.");

        alertDialog.setView(promptView);
        alertDialog.show();
//        alertDialog.getWindow().setBackgroundDrawable(new );


        if(shape == "HEART" || shape == "STAR" || shape == "CIRCLE"){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (result == null) {
                Log.e("**Fresult","**Fresult");
                return;
            }

            Uri picUri = result.getUri();

            if (picUri == null) {
                Log.e("**Sresult","**Sresult");
                return;
            }
            Bitmap bitmap;
            try {

                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), picUri);
                bitmap.setHasAlpha(true);

                Log.e("**onactivityresult","**onactivityresult"+bitmap);
                if (bitmap == null) {
                    Log.e("**Bresult","**Bresult");
                    return;
                }
                Bitmap bit;

                if(shape =="HEART"){
                     bit=CropImage.toHeartBitmap(bitmap, this);
                }
                else if(shape =="CIRCLE"){
                    bit=CropImage.toCircleBitmap(bitmap, this);
                }
                else{
                    bit=CropImage.toStarBitmap(bitmap, this);
                }


                Log.e("bit:","bit"+bit);

                String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                        "/PhysicsSketchpad";
                File dir = new File(file_path);
                if(!dir.exists())
                    dir.mkdirs();
                File file = new File(dir, "sketchpad" + ".png");
                FileOutputStream fOut = new FileOutputStream(file);

                bit.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                fOut.flush();
                fOut.close();

                String filepath = file.getPath();
                IconUri=Uri.fromFile(new File(filepath));
                if(alertDialog != null) {
                    alertDialog.dismiss();
                }

                //                IconUri = getImageUri(EditStickersActivity.this, bit);
                Log.e("**getUriEdit","**getUriEdit"+data);
//
//  callEditor(IconUri);
//                IconUri=CutOut.getUri(data);

                imageView2.setImageBitmap(bit);//working code

                    imageView2.setImageURI(IconUri);
            } catch (IOException e) {
                Log.e("**Eresult","**Eresult");
                e.printStackTrace();
            }

        }

        if (requestCode == ADD_PACK){
        }
        else if(requestCode == 3000){
            if(data!=null){
                Log.e("tis","is"+data.getClipData());
                if(data.getClipData()!=null){
                    ClipData clipData = data.getClipData();
                    for(int i = 0; i < clipData.getItemCount(); i++)
                    {
                        ClipData.Item path = clipData.getItemAt(i);
                        imageIconUri = path.getUri();
                        getContentResolver().takePersistableUriPermission(Objects.requireNonNull(imageIconUri), Intent.FLAG_GRANT_READ_URI_PERMISSION);

                        imageView2.setImageURI(imageIconUri);
                        //stickerPack.addSticker(uri, this);
                    }
                } else {
                    imageIconUri = data.getData();
                    getContentResolver().takePersistableUriPermission(Objects.requireNonNull(imageIconUri), Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    imageView2.setImageURI(imageIconUri);
//                    alertDialog.dismiss();
                    //stickerPack.addSticker(uri, this);
                }
//                finish();
//                startActivity(getIntent());
            }

        }
        else if (requestCode == CutOut.CUTOUT_ACTIVITY_REQUEST_CODE) {

            switch (resultCode) {
                case Activity.RESULT_OK:
                    System.out.print("Activity.RESULT_OK");
                    IconUri = CutOut.getUri(data);
                    // Save the image using the returned Uri here
                    imageView2.setImageURI(IconUri);
                    imageView2.setTag(IconUri);
//                    savefile(imageUri);
                    break;
                case CutOut.CUTOUT_ACTIVITY_RESULT_ERROR_CODE:
                    System.out.print("CUTOUT_ACTIVITY_RESULT_ERROR_CODE");
                    Exception ex = CutOut.getError(data);
                    break;
                default:
                    System.out.print("User cancelled the CutOut screen");
            }
        }
//        else {

//            if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
//           final Uri resultUri = UCrop.getOutput(data);
//           Log.e("rews","o"+resultUri);
//       } else if (resultCode == UCrop.RESULT_ERROR) {
//           final Throwable cropError = UCrop.getError(data);
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR1)
    public Uri getImageUri(Context context, Bitmap inImage) {
//        getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.setHasAlpha(true);
        inImage.compress(Bitmap.CompressFormat.PNG, 90, bytes);
        Log.e("**im","**j"+inImage);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        Log.e("**fg","d"+path);
        return Uri.parse(path);
    }

//public void callEditor(Uri uri){
//
//}
    private void openFile() {
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        i.setType("image/*");
        startActivityForResult(i, 3000);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.eraser){
//            String path = imageView2.getTag().toString();
            Log.e("D","cut"+imageIconUri);
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            final Uri uri = getUriFromDrawable(R.drawable.githublogo);
//            Log.e("e","d"+uri);

            CutOut.activity().src(imageIconUri).start(this);
//                CutOut.activity()
//                        .src(Uri.parse(path))
//
//                        .bordered()
//                        .noCrop()
//                        .intro()
//                        .start(this);

        }else if(v.getId()==R.id.circle){
            shape="CIRCLE";
            cropImage();

        }else if(v.getId()==R.id.heart){
            shape="HEART";
            cropImage();

        }
        else if(v.getId()==R.id.square){
            shape="STAR";
            cropImage();

        }else{

        }
    }

    private void cropImage() {

        if(shape == "HEART"){
            CropImage.activity(imageIconUri).setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.HEART)
                    .start(EditStickersActivity.this);
        }
        else if(shape == "CIRCLE"){
            CropImage.activity(imageIconUri).setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.CIRCLE)
                    .start(EditStickersActivity.this);
        }
        else{
            CropImage.activity(imageIconUri).setGuidelines(CropImageView.Guidelines.ON)
                    .setCropShape(CropImageView.CropShape.STAR)
                    .start(EditStickersActivity.this);
        }

    }

    void savefile(Uri sourceuri)
    {
        String sourceFilename= sourceuri.getPath();
        String destinationFilename = android.os.Environment.getExternalStorageDirectory().getPath()+ File.separatorChar+"abc.png";

        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(sourceFilename));
            bos = new BufferedOutputStream(new FileOutputStream(destinationFilename, false));
            byte[] buf = new byte[1024];
            bis.read(buf);
            do {
                bos.write(buf);
            } while(bis.read(buf) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public Uri getUriFromDrawable(int drawableId) {
        System.out.print("********************getUriFromDrawable");


        return Uri.parse("android.resource://" + getPackageName() + "/drawable/" + getApplicationContext().getResources().getResourceEntryName(drawableId));
    }
}
