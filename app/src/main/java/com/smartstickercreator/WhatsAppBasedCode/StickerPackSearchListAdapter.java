package com.smartstickercreator.WhatsAppBasedCode;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.smartstickercreator.R;

import java.util.ArrayList;
import java.util.List;

public class StickerPackSearchListAdapter extends RecyclerView.Adapter<StickerPackSearchListItemViewHolder> {
    @NonNull
    private List<StickerSearchPack> stickerPacks;
    ArrayList<Sticker2> list=new ArrayList<Sticker2>();
    List<Uri> list2;
    SharedPreferences pref;
    SharedPreferences.Editor mEditor;
    @NonNull
    private final OnSearchButtonClickedListener onAddButtonsearchClickedListener;
    private int maxNumberOfStickersInARow;

    StickerPackSearchListAdapter(@NonNull List<StickerSearchPack> stickerPacks, @NonNull OnSearchButtonClickedListener onAddButtonClickedListener) {
        this.stickerPacks = stickerPacks;
        this.onAddButtonsearchClickedListener = onAddButtonClickedListener;
    }

    @NonNull
    @Override
    public StickerPackSearchListItemViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        final Context context = viewGroup.getContext();
        final LayoutInflater layoutInflater = LayoutInflater.from(context);
        final View stickerPackRow = layoutInflater.inflate(R.layout.stickerpack_searchlist_item, viewGroup, false);
        return new StickerPackSearchListItemViewHolder(stickerPackRow);
    }

    @Override
    public void onBindViewHolder(@NonNull final StickerPackSearchListItemViewHolder viewHolder, final int index) {
        StickerSearchPack pack = stickerPacks.get(index);
        final Context context = viewHolder.publisherView.getContext();
//        Log.e("sticker", "pk" + pack.getStickers());
//        list=pack.getListuri();
        list2=pack.getListuri();

        if(pack.publisher==null){
            viewHolder.publisherView.setText("by " +"SmartSticker User");
        }else {

            viewHolder.publisherView.setText("by " + pack.publisher);
        }
//        //viewHolder.filesizeView.setText(Formatter.formatShortFileSize(context, pack.getTotalSize()));
//
        viewHolder.titleView.setText(pack.name);

        viewHolder.container.setOnClickListener(view -> {
            StickerSearchPack pack2 = stickerPacks.get(index);
            list=new ArrayList<Sticker2>();
//            list.addAll(stickerPacks.get(index).getStickers());
            Log.e("po","o"+pack2.getTrayImageUri());
//            Log.e("po","o"+pack2.getSticker(0).getUri());
            Bundle bundle=new Bundle();
            Log.e("lis","i"+stickerPacks.get(index).getSticker(0).getUri());
            bundle.putParcelable("searchPack",pack2);
            Intent intent = new Intent(viewHolder.container.getContext(), SearchStickerPackDetailsActivity.class);
            intent.putExtra("searchPack",pack2);
            intent.putExtra("Packid",pack2.getIdentifier());
            intent.putExtra("download",pack2.getIsDownlonload());
            intent.putExtra("name",pack2.getName());
            intent.putExtra("trayimage",pack2.getTrayImageUri());
            intent.putExtra("index",index);
            intent.putParcelableArrayListExtra("list", (ArrayList<Uri>) pack2.getListuri());
//            intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_DATA, pack.identifier);
            viewHolder.container.getContext().startActivity(intent);
        });
        viewHolder.imageRowView.removeAllViews();
        int actualNumberOfStickersToShow=4;
//        //if this sticker pack contains less stickers than the max, then take the smaller size.
//        int actualNumberOfStickersToShow = Math.min(maxNumberOfStickersInARow, pack.getStickers().size());
        for (int i = 0; i < actualNumberOfStickersToShow; i++) {

            final ImageView rowImage = (ImageView) LayoutInflater.from(context).inflate(R.layout.sticker_down_list_item_image, viewHolder.imageRowView, false);
//            rowImage.setImageURI(Uri.parse("https://www.smashbros.com/wiiu-3ds/images/character/toon_link/screen-7.jpg"));
            Glide.with(context).load(list2.get(i)).asBitmap().error(R.drawable.stickermakerlogo).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(rowImage);
//        Picasso.with(context).load("https://www.smashbros.com/wiiu-3ds/images/character/toon_link/screen-7.jpg").into(rowImage);
// rowImage.setImageURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/smart-stickers.appspot.com/o/Merry%20Christmas%2Fnm16.webp?alt=media&token=99af9886-36fe-4f92-a824-8ebb1e3f2d57"));
            final LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) rowImage.getLayoutParams();

            final int marginBetweenImages = (viewHolder.imageRowView.getMeasuredWidth() - maxNumberOfStickersInARow * viewHolder.imageRowView.getContext().getResources().getDimensionPixelSize(R.dimen.sticker_pack_list_item_preview_image_size)) / (maxNumberOfStickersInARow - 1) - lp.leftMargin - lp.rightMargin;
            if (i != actualNumberOfStickersToShow - 1 && marginBetweenImages > 0) { //do not set the margin for the last image
                lp.setMargins(lp.leftMargin, lp.topMargin, lp.rightMargin + marginBetweenImages, lp.bottomMargin);
                rowImage.setLayoutParams(lp);
            }
            viewHolder.imageRowView.addView(rowImage);
        }
//        setAddButtonAppearance(viewHolder.addButton, pack, index,pack.name);

//        viewHolder.shareButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DataArchiver.createZipFileFromStickerPack(pack, context);
//            }
//        });
    }

    private void setAddButtonAppearance(ImageView addButton, StickerSearchPack pack,int i,String packname) {
        if (pack.getIsWhitelisted()) {
            addButton.setImageResource(R.drawable.sticker_3rdparty_added);
            addButton.setClickable(false);
            addButton.setOnClickListener(null);
            addButton.setBackgroundDrawable(null);
        } else {

//            if(pack.IsDownlonload){
////                Log.e("down","true");
//                addButton.setVisibility(View.INVISIBLE);
//            }else {
//                Log.e("down","false");
//                addButton.setOnClickListener(v -> onAddButtonsearchClickedListener.onAddButtonClicked(pack,i,packname));
//                TypedValue outValue = new TypedValue();
//                addButton.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
//                addButton.setBackgroundResource(outValue.resourceId);

//            }
//            addButton.setImageResource();

        }
    }

    @Override
    public int getItemCount() {
        return stickerPacks.size();
    }

    void setMaxNumberOfStickersInARow(int maxNumberOfStickersInARow) {
        if (this.maxNumberOfStickersInARow != maxNumberOfStickersInARow) {
            this.maxNumberOfStickersInARow = maxNumberOfStickersInARow;
            notifyDataSetChanged();
        }
    }

    public interface OnSearchButtonClickedListener {
        void onAddButtonClicked(StickerSearchPack stickerPack,int i, String packname);
    }
}
