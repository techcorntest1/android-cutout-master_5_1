/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.smartstickercreator.WhatsAppBasedCode;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.smartstickercreator.R;

import java.io.File;

public class StickerPreviewAdapter extends RecyclerView.Adapter<StickerPreviewViewHolder> {

    @NonNull
    private StickerPack stickerPack;

    private final int cellSize;
    private int cellLimit;
    private int cellPadding;
    private final int errorResource;
    private final Activity activity;
    private final LayoutInflater layoutInflater;
    int imageHeight,imageWidth;
    StickerPreviewAdapter(final Activity activity,
                          @NonNull final LayoutInflater layoutInflater,
                          final int errorResource,
                          final int cellSize,
                          final int cellPadding,
                          @NonNull final StickerPack stickerPack) {
        this.cellSize = cellSize;
        this.cellPadding = cellPadding;
        this.cellLimit = 0;
        this.layoutInflater = layoutInflater;
        this.errorResource = errorResource;
        this.stickerPack = stickerPack;
        this.activity=activity;
    }

    @NonNull
    @Override
    public StickerPreviewViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        View itemView = layoutInflater.inflate(R.layout.sticker_image, viewGroup, false);
        StickerPreviewViewHolder vh = new StickerPreviewViewHolder(itemView);
        Sticker thisSticker = stickerPack.getSticker(i);
        DisplayMetrics dis=new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dis);
        int original_we =dis.widthPixels;
        int original_he=dis.heightPixels;
        vh.frme.setMinimumWidth(original_we/3-R.dimen.preview_side_margin-R.dimen.preview_side_margin);
        getImageWidthAndHeight(thisSticker.getUri());
//         int ratio = Math.min(original_we / imageWidth, original_he / imageHeight);
//        int new_he=(original_we*imageHeight/imageWidth);
//        Log.e("size","is"+imageHeight +"  "+imageHeight +cellSize);
        ViewGroup.LayoutParams layoutParams = vh.stickerPreviewView.getLayoutParams();
        layoutParams.height =cellSize;
        layoutParams.width =cellSize ;
        vh.stickerPreviewView.setLayoutParams(layoutParams);
//        vh.stickerPreviewView.setPadding(5,5,5, 5);
        return vh;
    }
    private void getImageWidthAndHeight(Uri uri){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
        imageHeight = options.outHeight;
        imageWidth = options.outWidth;

    }
    @Override
    public void onBindViewHolder(@NonNull final StickerPreviewViewHolder stickerPreviewViewHolder, final int i) {
        Sticker thisSticker = stickerPack.getSticker(i);
        Context thisContext = stickerPreviewViewHolder.stickerPreviewView.getContext();
        stickerPreviewViewHolder.stickerPreviewView.setImageResource(errorResource);
        stickerPreviewViewHolder.stickerPreviewView.setImageURI(thisSticker.getUri());
        stickerPreviewViewHolder.stickerPreviewView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView image = new ImageView(thisContext);
                image.setImageURI(thisSticker.getUri());
//                Log.e("e","i"+thisSticker.getUri());
//                Intent intent = new Intent(activity, PreviewSticker.class);
//                activity.startActivity(intent);
//                thisContext.PreviewSticke();
//                LayoutInflater layoutInflater = LayoutInflater.from(thisContext);
//                View promptView = layoutInflater.inflate(R.layout.preview, null);
//
//                final AlertDialog alertD = new AlertDialog.Builder(thisContext).create();
//
//                TextView userInput = (TextView) promptView.findViewById(R.id.userInput);
//                TextView desc = (TextView) promptView.findViewById(R.id.userInput);
//                Button btnAdd1 = (Button) promptView.findViewById(R.id.btnAdd1);
//
//                Button btnAdd2 = (Button) promptView.findViewById(R.id.btnAdd2);
//
//                btnAdd1.setOnClickListener(new View.OnClickListener() {
//                    public void onClick(View v) {
//
//                        // btnAdd1 has been clicked
//
//                    }
//                });
//
//                btnAdd2.setOnClickListener(new View.OnClickListener() {
//                    public void onClick(View v) {
//
//                        // btnAdd2 has been clicked
//
//                    }
//                });
//
//                alertD.setView(promptView);
//
//                alertD.show();



                AlertDialog alertDialog = new AlertDialog.Builder(thisContext)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(stickerPack.getStickers().size()>3 || !WhitelistCheck.isWhitelisted(thisContext, stickerPack.getIdentifier())){
                                    dialogInterface.dismiss();
                                    stickerPack.deleteSticker(thisSticker);
                                    Activity thisActivity = ((Activity)thisContext);
                                    thisActivity.finish();
                                    thisActivity.startActivity(thisActivity.getIntent());
                                    Toast.makeText(thisContext, "Sticker deleted", Toast.LENGTH_SHORT).show();
                                } else {
                                    AlertDialog alertDialog = new AlertDialog.Builder(thisContext)
                                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            }).create();
                                    alertDialog.setTitle("Invalid Action");
                                    alertDialog.setMessage("A sticker pack that is already applied to WhatsApp cannot have less than 3 stickers. " +
                                            "In order to remove additional stickers, please add more to the pack first or remove the pack from the WhatsApp app.");
                                    alertDialog.show();
                                }
                            }
                        })
                        .setView(image)
                        .create();
                alertDialog.setTitle("Are yoy sure Delete sticker from the Pack?");
                alertDialog.setMessage("Deleting this sticker will delete from WhatsApp Automatically.");
                alertDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        int numberOfPreviewImagesInPack;
        numberOfPreviewImagesInPack = stickerPack.getStickers().size();
        if (cellLimit > 0) {
            return Math.min(numberOfPreviewImagesInPack, cellLimit);
        }
        return numberOfPreviewImagesInPack;
    }
}
