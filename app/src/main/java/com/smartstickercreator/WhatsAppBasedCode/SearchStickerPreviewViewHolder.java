/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.smartstickercreator.WhatsAppBasedCode;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.smartstickercreator.R;

public class SearchStickerPreviewViewHolder extends RecyclerView.ViewHolder {

//    public ImageView stickerPreviewView;
        SimpleDraweeView search_sticker_preview;
    public FrameLayout frme;

    SearchStickerPreviewViewHolder(final View itemView) {
        super(itemView);
        search_sticker_preview=itemView.findViewById(R.id.search_sticker_preview);
//        stickerPreviewView = itemView.findViewById(R.id.search_sticker_preview);
        frme=itemView.findViewById(R.id.search_frme);
    }
}