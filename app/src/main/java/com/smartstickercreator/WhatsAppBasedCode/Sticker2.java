package com.smartstickercreator.WhatsAppBasedCode;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
public class Sticker2 implements Parcelable {
    String imageFileName;
    List<String> emojis;
    Uri uri;
    long size;

    public Sticker2(String imageFileName, List<String> emojis) {
        this.imageFileName = imageFileName;
        this.emojis = emojis;
    }
    public Sticker2(String imageFileName, Uri uri) {
        this.imageFileName = imageFileName;
        this.uri = uri;
    }
    public Sticker2(String imageFileName, Uri uri, List<String> emojis) {
        this.imageFileName = imageFileName;
        this.emojis = emojis;
        this.uri = uri;
    }

    public Sticker2(Parcel in) {
        imageFileName = in.readString();
        emojis = in.createStringArrayList();
        size = in.readLong();
    }


    public static final Creator<Sticker2> CREATOR = new Creator<Sticker2>() {
        @Override
        public Sticker2 createFromParcel(Parcel in) {
            return new Sticker2(in);
        }

        @Override
        public Sticker2[] newArray(int size) {
            return new Sticker2[size];
        }
    };

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(imageFileName);
        dest.writeStringList(emojis);
        dest.writeLong(size);
    }

    public Uri getUri() {
        return uri;
    }

    public String getImageFileName() {
        return imageFileName;
    }
}
