package com.smartstickercreator.WhatsAppBasedCode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.smartstickercreator.R;

public class ImageCropActivity extends AppCompatActivity {
    ImageView compositeImageView;
    boolean crop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_crop);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            crop = extras.getBoolean("crop");
        }
        int widthOfscreen = 0;
        int heightOfScreen = 0;

        DisplayMetrics dm = new DisplayMetrics();
        try {
            getWindowManager().getDefaultDisplay().getMetrics(dm);
        } catch (Exception ex) {
        }
        widthOfscreen = dm.widthPixels;
        heightOfScreen = dm.heightPixels;

        compositeImageView = (ImageView) findViewById(R.id.iv);

        Bitmap bitmap2 = null;

        byte[] byteArray = getIntent().getByteArrayExtra("image");

        Log.e("byre","c"+byteArray);

        try {
            bitmap2 = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            Log.e("nii","f"+bitmap2);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(),
//                R.drawable.redditlogo);

        Bitmap resultingImage = Bitmap.createBitmap(widthOfscreen,
                heightOfScreen, bitmap2.getConfig());

        Canvas canvas = new Canvas(resultingImage);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        canvas.drawColor(getResources().getColor(R.color.colorPrimary),PorterDuff.Mode.CLEAR);
        Path path = new Path();
        Log.e("crop","view"+CropView.points);
        for (int i = 0; i < CropView.points.size(); i++) {
            path.lineTo(CropView.points.get(i).x, CropView.points.get(i).y);
        }
        canvas.drawPath(path, paint);
//        canvas.clipPath(path);
        if (crop) {
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        } else {
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));
        }
        canvas.drawBitmap(bitmap2, 0, 0, paint);
//        canvas.drawBitmap(bitmap2, 8, 350, paint);
        compositeImageView.setImageBitmap(resultingImage);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
