package com.smartstickercreator.WhatsAppBasedCode;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.smartstickercreator.BuildConfig;
import com.smartstickercreator.DataArchiver;
import com.smartstickercreator.PreLoader;
import com.smartstickercreator.R;
import com.smartstickercreator.SearchBook;
import com.smartstickercreator.StickerBook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.smartstickercreator.SearchBook.addStickerPackExisting;
import static com.smartstickercreator.StickerBook.getAllStickerPacks;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackDetailsActivity.EXTRA_STICKER_PACK_AUTHORITY;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackDetailsActivity.EXTRA_STICKER_PACK_ID;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackDetailsActivity.EXTRA_STICKER_PACK_NAME;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackListActivity.getImageUri;
import static java.lang.Thread.sleep;

public class StickerPackSearchList extends BaseActivity {
//    public static String API_URL = "http://erronak.com/dev/smartstickers/";
public static String API_URL = "https://firebasestorage.googleapis.com/v0/b/smart-stickers.appspot.com/o/contents.json?alt=media&token=ccb08572-f475-4e1a-8b6a-a505eb80fc6c";
    int count = 0;
    private static final int STICKER_PREVIEW_DISPLAY_LIMIT = 5;
    ArrayList<StickerSearchPack> arrStickerSearchBook;
    private RecyclerView packRecyclerView;
    private LinearLayoutManager packLayoutManager;
    private StickerPackSearchListAdapter allStickerPacksListAdapter;
    File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
    public static ArrayList<StickerSearchPack> list = new ArrayList<StickerSearchPack>();
    public JSONArray arr2;
    private long downloadReference;
    DownloadManager downloadManager;
    public static ProgressDialog progressDialog,progressDialog2;
    public  ProgressDialog progress;
    String id;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    private long fileSize = 0;
    static FirebaseStorage storage;
    List<String> lists = new ArrayList<String>();
    int searchStickers=0;
    private InterstitialAd mInterstitialAd;
    static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.sticker_pack_search);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());
        FirebaseApp.initializeApp(StickerPackSearchList.this);
         storage = FirebaseStorage.getInstance("gs://smart-stickers.appspot.com");

//        Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();
//        String generatedFilePath = downloadUri.toString(); /// The string(file link) that you need

//        downloadFile();
        packRecyclerView = findViewById(R.id.sticker_pack_searchlist);
        SearchBook.init(StickerPackSearchList.this);
        arrStickerSearchBook = SearchBook.checkIfPacksAreNull();
        progressDialog = new ProgressDialog(StickerPackSearchList.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.setTitle("DownLoading");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBarStatus = 0;
        fileSize = 0;
//         progressBar = new ProgressBar(StickerPackSearchList.this,null,android.R.attr.progressBarStyleSmall);
//         progressBar.setIndeterminate(true);
        if (arrStickerSearchBook.size() == 0) {
            Log.e("len", "0");
            new RetrieveFeedTask().execute();
        } else if (arrStickerSearchBook.size() > 0) {
            String list = null;
            ArrayList<StickerPack> arrlist=getAllStickerPacks();
            ArrayList<StickerSearchPack> arrayList=new ArrayList<StickerSearchPack>();
            arrayList=arrStickerSearchBook;
            ArrayList<StickerPack> len2=arrlist;
            int lensz=arrayList.size();
            for (int i = 0; i < arrayList.size(); i++) {
                arrayList.get(i).setIsDownlonload(false);
//                        StickerSearchPack searchlistdown=SearchBook.getStickerPackById(arrayList.get(i).getIdentifier());
//                        Log.e("main","o"+searchlistdown);
//                        int pos=SearchBook.getStickerPackByPos(arrayList.get(i).getIdentifier());
//                        SearchBook.downloadpack(pos,false);

            }
            for (int i = 0; i < len2.size(); i++) {

                for (int j = 0; j < lensz; j++) {

                    if (len2.get(i).getIdentifier().contentEquals(arrayList.get(j).getIdentifier())){

//                                    SearchBook.downloadpack(j,true);
//                                arrayList.remove(j);
//                                lensz=arrayList.size();

                        list=arrayList.get(j).getIdentifier();
//                                if(!arrayList.get(j).getIdentifier().equalsIgnoreCase(list)){
                                    Log.e("li-----","i"+j);
                        SearchBook.downloadpack(j,true);
//                                }

                    }
                    else{
//                                Log.e("li","i"+len2.get(i).getIdentifier());
//                                SearchBook.downloadpack(j,false);
                    }
                }
            }

            ArrayList<StickerSearchPack> lsp = DataArchiver.readsearchPackJSON(StickerPackSearchList.this);
            showStickerPackList(lsp);
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
         boolean down=getIntent().getBooleanExtra("downloadPack",false);
        if(down){
            Bundle b1=getIntent().getBundleExtra("pack");
            Log.e("B1","ui"+b1.getParcelable("uri"));
            int index=getIntent().getIntExtra("index",0);
            String nm=getIntent().getStringExtra("packNm");
            StickerSearchPack searchPack=b1.getParcelable("Pack");
            Uri trayuri=b1.getParcelable("uri");
            Log.e("44","ui"+index+nm+searchPack.getIdentifier());
            new getBitmap2(searchPack,index,nm,trayuri).execute();
        }
//        register  a receiver
//
//        BroadcastReceiver receiver = new BroadcastReceiver(){
////            String uriString;
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                String action = intent.getAction();
//                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
//                    long downloadId = intent.getLongExtra(
//                            DownloadManager.EXTRA_DOWNLOAD_ID, 0);
//                    DownloadManager.Query query = new DownloadManager.Query();
//                    query.setFilterById(downloadReference);
//                    Cursor c = downloadManager.query(query);
//                    if (c.moveToFirst()) {
//                        int columnIndex = c
//                                .getColumnIndex(DownloadManager.COLUMN_STATUS);
//                        if (DownloadManager.STATUS_SUCCESSFUL == c
//                                .getInt(columnIndex)) {
//
//                            String uriString = c
//                                    .getString(c
//                                            .getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
//
//                            Log.e("uri","i"+Uri.parse(uriString));
//
//                        }
//
////                        StickerPack stickerPack = new StickerPack(currentPack.getIdentifier(),currentPack.getName(), currentPack.getPublisher(), currentPack.getTrayImageUri(), "", "", "", "", StickerPackSearchList.this);
////                        stickerPack.addSticker(Uri.parse(uriString),StickerPackSearchList.this);
//
//                    }
////                    Log.e("down","id"+downloadId+uriString);
//
//
//                }
//
//            }
//        };
//
//        registerReceiver(receiver, new IntentFilter(
//                DownloadManager.ACTION_DOWNLOAD_COMPLETE));

    }
    private void downloadFile() {
//



        StorageReference islandRef2 = storage.getReferenceFromUrl("gs://smart-stickers.appspot.com").child("Heart/calls.webp");

//        StorageReference islandRef = storage.getReferenceFromUrl("https://firebasestorage.googleapis.com/v0/b/smart-stickers.appspot.com/o/Heart%2Ftray.png?alt=media&token=53b02c43-764d-484a-a98c-593835929b03");
//        StorageReference  islandRef = storageRef.child("https://firebasestorage.googleapis.com/v0/b/smart-stickers.appspot.com/o/Heart%2Ftray.png?alt=media&token=53b02c43-764d-484a-a98c-593835929b03");

        File rootPath = new File(Environment.getExternalStorageDirectory().getAbsoluteFile(), "abc");
        if(!rootPath.exists()) {
            rootPath.mkdirs();
        }

        final File localFile = new File(rootPath,"tray.png");

        islandRef2.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.e("firebase ",";local tem file created  created " +localFile.toString());
                //  updateDb(timestamp,localFile.toString(),position);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.e("firebase ",";local tem file not created  created " +exception.toString());
            }
        });
    }
    public void showPogress(){
        progressDialog = new ProgressDialog(StickerPackSearchList.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
//        progressDialog.setTitle("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.getProgress();
        progressDialog.incrementProgressBy(20);
        progressDialog.setProgress(0);
        progressDialog.setMax(100);
        progressDialog.show();
    }
    public int doOperation() {
        //The range of ProgressDialog starts from 0 to 10000
        while (fileSize <= 10000) {
            fileSize++;
            if (fileSize == 1000) {
                return 10;
            } else if (fileSize == 2000) {
                return 20;
            } else if (fileSize == 3000) {
                return 30;
            } else if (fileSize == 4000) {
                return 40; // you can add more else if
            }
         /* else {
                return 100;
            }*/
        }//end of while
        return 100;
    }//end of doOperation

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;
        private String version;

        //
        protected void onPreExecute() {
            progressDialog2 = new ProgressDialog(StickerPackSearchList.this);
            progressDialog2.setCancelable(false);
            progressDialog2.setMessage("Synchronizing Data..");
//        progressDialog.setTitle("Processing...");
            progressDialog2.setCancelable(false);
            progressDialog2.setIndeterminate(true);
            progressDialog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog2.setMax(100);
            progressDialog2.show();
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
//            String email = emailText.getText().toString();
            // Do some validation here

            try {
                URL url = new URL(API_URL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
//            progressBar.setVisibility(View.GONE);
//            Log.i("INFO", response);
            try {

                JSONObject objMAin = new JSONObject(response);
                version = objMAin.getString("version");

                Log.e("ver", "is" + version);
                JSONArray arr = objMAin.getJSONArray("packs");
                JSONObject obj = (JSONObject) arr.get(0);
                arr2 = obj.getJSONArray("sticker_packs");
//                for(int i=0;i<arr2.length();i++){
//                    JSONObject objpack=arr2.getJSONObject(i);
//                    Log.e("obj","pack"+objpack);
//                    String urlMain = objpack.getString("tray_image_file");
                new getBitmap1().execute(arr2);

//                    new getBitmap1(arr2).execute(arr2.toString());
//                }

                Log.e("listf", "of" + list);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
    public void downloadTray(String urlMain,JSONObject objpack ,int i){

        StorageReference islandRef2 = storage.getReference();

        final StorageReference downloadRef;
        downloadRef = islandRef2.child(urlMain);
//                            getRoot()
//                              StorageReference islandRef2 = storage.getReferenceFromUrl("gs://smart-stickers.appspot.com").child(urlMain);
        File rootPath = new File(Environment.getExternalStorageDirectory().getAbsoluteFile(), "smartStickers");

        if(!rootPath.exists()) {
            rootPath.mkdirs();
        }

        final File localFile =new File(rootPath,"downloadTray"+count +".png");
        count++;

        downloadRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.e("firebase ",";local tem file created  created " +Uri.parse(String.valueOf(localFile)));
                Uri uri= Uri.fromFile(localFile);
//                Log.e("***objpack","objpack"+objpack);
                setImage(uri,objpack,i);
                //  updateDb(timestamp,localFile.toString(),position);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.e("firebase ",";local tem file not created  created " +exception.toString());
            }
        });
    }
    public class getBitmap1 extends AsyncTask<JSONArray, Void, String> {

        private Exception exception;
        private String version;
        String url1 = "", filepath = null, abs;

        JSONObject objpack;
        JSONArray arr2;
        String  stickerpath = null, id;
        StickerPack stickerpack;


        int total, i,stickerCount=0;

        public getBitmap1() {
        }

        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(JSONArray... urls) {
            arr2 = urls[0];
            // Do some validation here
//            arr2=urls[0];
            Log.e("arr", "is" + arr2);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {

                        for (int i = 0; i < arr2.length(); i++) {
                            Uri uri;
                            JSONObject objpack = arr2.getJSONObject(i);
                            String id=objpack.getString("identifier");
                            String urlMain = objpack.getString("tray_image_file");
                            downloadTray(urlMain,objpack,i);

//

//

//                    new UnZipTask().execute(StorezipFileLocation, DirectoryName);



//              StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), contentUri, "", "", "", "", StickerPackListActivity.this);
//                    for (int j = 0; j < jsonArrayarr.length(); j++) {
//                        String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
////                    File folder = new File(stickers);
////                    new getBitmap2(stickers,stickerPack).execute();
////                           Uri stickUri =Uri.parse(path);
////                            Log.e("S", "p" + Uri.fromFile(folder));
//                    }









                        }
//                    catch (MalformedURLException e) {
//                        e.printStackTrace();
//                    }
//                    catch (IOException e) {
//                        filepath = null;
//                        e.printStackTrace();
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }

//            Log.i("filepath:"," "+filepath);
                }
            });
            return null;
//             catch (IOException e) {
//                // Log exception
//                return null;
//            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }

            try {
//                Log.e("im","j"+Uri.fromFile(new File(response)) +"**pk       "+objpack);
//                StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), Uri.fromFile(new File(response)), "", "", "", "", PreLoader.this);
//                JSONArray jsonArrayarr = objpack.getJSONArray("stickers");
//                Log.e("s", "p" + stickerPack);
////              StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), contentUri, "", "", "", "", StickerPackListActivity.this);
//                for (int j = 0; j < jsonArrayarr.length(); j++) {
//                    String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
////                    File folder = new File(stickers);
////                    new getBitmap2(stickers,stickerPack).execute();
////                           Uri stickUri =Uri.parse(path);
////                            Log.e("S", "p" + Uri.fromFile(folder));
//                }
//                list.add(stickerPack);
//                addStickerPackExisting(stickerPack);
//                passIntent(stickerPack,count);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }
    public void setImage(Uri uri2,JSONObject objpack,int i){
    Log.e("filepay","is"+uri2);
//        for(int k=0;k<arrpacks.length();k++){
////
//            String tmppckname=arrpacks.getJSONObject(k).getString("identifier");
//            if(tmppckname.equalsIgnoreCase(packname)) {
//                Log.e("chk:2:", "chk:2:" + arrpacks.getJSONObject(k).getString("identifier") + "%%%" + packname);
//                JSONObject objarr = arrpacks.getJSONObject(k);
        try {
//for display stickers in search-list
            JSONArray jsonArrayarr = objpack.getJSONArray("stickers");
//      String id = objpack.getString("identifier");
        List<Uri> flist = new ArrayList<Uri>();
           List<Uri> listuri =new ArrayList<Uri>();

                for(int k=0;k<jsonArrayarr.length();k++) {

//                    String tmppckname = jsonArrayarr.getJSONObject(k).getString("identifier");
//                    if (tmppckname.equalsIgnoreCase(id)) {
////                        Log.e("chk:2:", "chk:2:" + jsonArrayarr.getJSONObject(k).getString("identifier") + "%%%" + id);
//                        JSONObject objarr = jsonArrayarr.getJSONObject(k);
                    try{

                        String stickers = jsonArrayarr.getJSONObject(k).getString("image_file");
                        Log.e("stick","id"+stickers);
                        StorageReference islandRef2 = storage.getReference();
                        final StorageReference downloadRef;
                        downloadRef=islandRef2.getRoot().child(stickers);
                        Log.e("storage","ref");


                        int finalK = k;
                        downloadRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {

                                listuri.add(uri);
                                Log.e("li","i"+ finalK +(jsonArrayarr.length()-1));
                                if(finalK ==jsonArrayarr.length()-1){
//                                    setpack(listuri,objpack,uri2);
                                    try{
                                    //    for listing pack only
                                        Log.e("list","of uri"+listuri);
                                        StickerSearchPack stickerPack = new StickerSearchPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), uri2, "", "", "", "", StickerPackSearchList.this, false,listuri);
                                        for(int m=0;m<listuri.size();m++){
                                            Log.e("list","get"+listuri.get(m));
                                            stickerPack.addSticker(listuri.get(m),StickerPackSearchList.this);
                                        }

                                        stickerPack.setPublisher(objpack.getString("publisher"));
                                        stickerPack.setIdentifier(objpack.getString("identifier"));
                                        stickerPack.setName(objpack.getString("name"));
                                        stickerPack.setLicenseAgreementWebsite("");
                                        stickerPack.setPrivacyPolicyWebsite("");
                                        stickerPack.setPublisherEmail("");
                                        stickerPack.setPublisherWebsite("");
                                        stickerPack.setTrayImageUri(uri2, StickerPackSearchList.this);

                                        list.add(stickerPack);
                                        passIntent(count);

                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }


//                        uri2.add(listuri);

//                        Uri downloadUri = );
                                // Got the download URL for 'users/me/profile.png'
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle any errors
                            }
                        });

                        } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
//                    Log.e("li","is"+listuri);
//                    flist.addAll(listuri);

                    }
//        Log.e("laii","uri  "+listuri);

////

        } catch (JSONException e) {
            e.printStackTrace();
        }
}
//public void setpack(List<Uri> liuri, JSONObject objpack,Uri uri2){
//
//
//
//}
    public void passIntent(int total) {
        Log.e("list", "is" + list.size() + total);
//        DataArchiver.writeStickerBookJSON(list, PreLoader.this);
        if (list.size() == total) {
            progressDialog2.dismiss();
            Log.e("tot", "p" + (total) + list.size());
//
            DataArchiver.writesearchBookJSON(list, StickerPackSearchList.this);
//            ArrayList<StickerPack> arrList=StickerBook.getAllStickerPacks();
            showStickerPackList(list);

        }
    }
    public class getStickers extends AsyncTask<Void, Void, String> {

        private Exception exception;
        private String version,stickerpath=null,id;
        StickerPack stickerpack;
        int stickerCount=0;
        List<String> listnew = new ArrayList<String>();
        JSONArray jsonArrayarr;
        public getStickers(String id,StickerPack stickerpack,JSONArray jsonArrayarr){
            this.stickerpack=stickerpack;
            this.jsonArrayarr=jsonArrayarr;
            this.id=id;
        }
        protected void onPreExecute() {
            progressDialog.show();
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        @Override
        protected String doInBackground(Void... voids) {
            try{
            for (int j = 0; j < jsonArrayarr.length(); j++) {
                String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
                URL url = new URL(stickers);
                File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
                String filename = id + stickerCount + ".png";
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                stickerCount++;
                Log.i("Local filename:", "" + filename + stickerCount);
                File file = new File(SDCardRoot, filename);
                if (file.createNewFile()) {
                    file.createNewFile();
                }
                FileOutputStream fileOutput = new FileOutputStream(file);
                InputStream inputStream = urlConnection.getInputStream();
                int totalSize = urlConnection.getContentLength();
                int downloadedSize = 0;
                byte[] buffer = new byte[2048];
                int bufferLength = 0;
                long total = 0;
                InputStream input = new BufferedInputStream(
                        url.openStream(), 78000);
                while ((count = input.read(buffer)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
//                        publishProgress((int) ((total * 100)/totalSize));

                    // writing data to file
                    fileOutput.write(buffer, 0, count);
                }
                fileOutput.flush();
                input.close();
//                while ((bufferLength = inputStream.read(buffer)) > 0) {
//                    fileOutput.write(buffer, 0, bufferLength);
//                    downloadedSize += bufferLength;
//                    Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);
//                }
                fileOutput.close();
//                if (downloadedSize == totalSize)
                stickerpath = file.getPath();
                Log.i("filepath:444", " " + stickerpath);
////
                listnew.add(stickerpath);


                stickerpack.addSticker(Uri.fromFile(new File(stickerpath)), StickerPackSearchList.this);


            }
                Log.i("ciu", " " + stickerCount +"  "+ listnew.size());
                if (stickerCount == listnew.size()) {
                    StickerBook.addStickerPackExisting(stickerpack);
                }
            }catch(JSONException exception){
                    exception.printStackTrace();
                } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            progressDialog.dismiss();
//            progressBar.setVisibility(View.GONE);
//            Log.i("INFO", response);

        }
    }
    public void showStickerPackList(List<StickerSearchPack> stickerPackList) {
        Log.e("add","cshow"+stickerPackList.get(0).getSticker(0).getUri());
        allStickerPacksListAdapter = new StickerPackSearchListAdapter(stickerPackList, onAddButtonClickedListener);
        packRecyclerView.setAdapter(allStickerPacksListAdapter);
        packLayoutManager = new LinearLayoutManager(this);
        packLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                packRecyclerView.getContext(),
                packLayoutManager.getOrientation()
        );
        packRecyclerView.addItemDecoration(dividerItemDecoration);
        packRecyclerView.setLayoutManager(packLayoutManager);
        allStickerPacksListAdapter.notifyDataSetChanged();
//        packRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(this::recalculateColumnCount);
    }

    private void recalculateColumnCount() {
        final int previewSize = getResources().getDimensionPixelSize(R.dimen.sticker_pack_list_item_preview_image_size);
        int firstVisibleItemPosition = packLayoutManager.findFirstVisibleItemPosition();
        StickerPackSearchListItemViewHolder viewHolder = (StickerPackSearchListItemViewHolder) packRecyclerView.findViewHolderForAdapterPosition(firstVisibleItemPosition);
//        if (viewHolder != null) {
//            final int max = Math.max(viewHolder.imageRowView.getMeasuredWidth() / previewSize, 1);
//            int numColumns = Math.min(STICKER_PREVIEW_DISPLAY_LIMIT, max);
//            allStickerPacksListAdapter.setMaxNumberOfStickersInARow(numColumns);
//        }
    }

    public final class getBitmap2 extends AsyncTask<JSONArray, Void, String> {
        String url1 = "", stickerpath = null, id,packname=null;
        JSONObject objpack;
        JSONArray jsonArrayarr;
        StickerSearchPack pack;
        StickerPack stickerpack;
       int stickerCount = 0,pos;
       Uri uri,trayuri;



        public getBitmap2(StickerSearchPack pack,int pos,String packname,Uri uri) {
            this.pack = pack;
            this.pos=pos;
            this.packname=packname;
            this.trayuri=uri;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(JSONArray... urls) {

                    try {
//                        context=(Context) StickerPackSearchList.this;
                        URL url2 = new URL(API_URL);
                        HttpURLConnection urlConnection2 = (HttpURLConnection) url2.openConnection();

                        try {
//                            Log.e("current", "p" + pack.getTrayImageUri());
//                            trayuri=pack.getTrayImageUri();
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection2.getInputStream()));
                            StringBuilder stringBuilder = new StringBuilder();
                            String line;
                            while ((line = bufferedReader.readLine()) != null) {
                                stringBuilder.append(line).append("\n");
                            }
                            bufferedReader.close();
                            String response = stringBuilder.toString();
                            JSONObject objMAin = new JSONObject(response);
                            JSONArray arr = objMAin.getJSONArray("packs");
                            JSONObject obj = (JSONObject) arr.get(0);
                            JSONArray arrpacks = obj.getJSONArray("sticker_packs");

                            for(int k=0;k<arrpacks.length();k++){
//
                                        String tmppckname=arrpacks.getJSONObject(k).getString("identifier");
                                if(tmppckname.equalsIgnoreCase(packname)) {
                                    Log.e("chk:2:", "chk:2:" + arrpacks.getJSONObject(k).getString("identifier") + "%%%" + packname);
                                    JSONObject objarr = arrpacks.getJSONObject(k);

//                            JSONObject objarr = arrpacks.getJSONObject(pos);
                                    JSONArray jsonArrayarr = objarr.getJSONArray("stickers");
                                    String id = objarr.getString("identifier");
                                    Log.e("i", "id" + id);

                                    stickerpack = new StickerPack(pack.getIdentifier(), pack.getName(), pack.getPublisher(),trayuri , "", "", "", "", context);
//                        jsonArrayarr = urls[0];
//                         Log.e("id","o"+id+pack);
                                    for (int j = 0; j < jsonArrayarr.length(); j++) {
                                        String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
                                        StorageReference islandRef2 = storage.getReference();

                                        final StorageReference downloadRef;
                                        downloadRef = islandRef2.getRoot().child(stickers);
//                                  StorageReference islandRef2 = storage.getReferenceFromUrl("gs://smart-stickers.appspot.com").child(urlMain);
                                        File rootPath = new File(Environment.getExternalStorageDirectory().getAbsoluteFile(), "smartStickers");

                                        if (!rootPath.exists()) {
                                            rootPath.mkdirs();
                                        }

                                        final File localFile = new File(rootPath, id + stickerCount + ".png");
                                        stickerCount++;

                                        downloadRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                                Log.e("firebase ", ";local tem file created  created " + Uri.parse(String.valueOf(localFile)));
//                                    uri= Uri.parse(String.valueOf(localFile));
                                                uri = Uri.fromFile(localFile);
                                                stickerpath = String.valueOf(localFile);
                                                Log.e("dr", "i" + uri);
//                                    setStickers(uri,stickerpath,stickerpack);
                                                lists.add(String.valueOf(localFile));

                                                stickerpack.addSticker(uri, StickerPackSearchList.this);
//                                    stickerpack.getIdentifier()
                                                Log.e("lists", "apss" + lists.size() + " " + stickerpack.getStickers());
                                                if (stickerCount == lists.size()) {
                                                    StickerBook.init(StickerPackSearchList.this);
                                                    StickerBook.addStickerPackExisting(stickerpack);
                                                    progressDialog.dismiss();
                                                    AlertDialog.Builder builder = new AlertDialog.Builder(StickerPackSearchList.this);
                                                    builder.setMessage("Download Successful")
                                                            .setCancelable(true)
                                                            .setPositiveButton("ok", new DialogInterface.OnClickListener(){
                                                                public void onClick(DialogInterface dialog, int id){
                                                                    dialog.dismiss();
                                                                        Log.e("all","st"+StickerBook.getAllStickerPacks());
                                                                    Intent intent2 = new Intent(StickerPackSearchList.this, StickerPackListActivity.class);
                                                                    intent2.putExtra("val","newpack");
// intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                    startActivity(intent2);

//                                                                    Intent intent = new Intent();
//                                                                    intent.setAction("com.whatsapp.intent.action.ENABLE_STICKER_PACK");
//                                                                    intent.putExtra(EXTRA_STICKER_PACK_ID, stickerpack.identifier);
//                                                                    intent.putExtra(EXTRA_STICKER_PACK_AUTHORITY, BuildConfig.CONTENT_PROVIDER_AUTHORITY);
//                                                                    intent.putExtra(EXTRA_STICKER_PACK_NAME, stickerpack.name);


//                                                                       openFileTray(nameBox.getText().toString(), creatorBox);
                                                                }
                                                            });

                                                    AlertDialog alert = builder.create();
                                                    alert.show();

                                                }
                                                //  updateDb(timestamp,localFile.toString(),position);
                                            }
                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception exception) {
                                                Log.e("firebase ", ";local tem file not created  created " + exception.toString());
                                            }
                                        });

//                            URL url = new URL(stickers);
//                            File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
//                            String filename = id + stickerCount + ".png";
//                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//                            urlConnection.setRequestMethod("GET");
//                            urlConnection.setDoOutput(true);
//                            urlConnection.connect();
//                            stickerCount++;
//                            Log.i("Local filename:", "" + filename + stickerCount);
//                            File file = new File(SDCardRoot, filename);
//                            if (file.createNewFile()) {
//                                file.createNewFile();
//                            }
//                            FileOutputStream fileOutput = new FileOutputStream(file);
//                            InputStream inputStream = urlConnection.getInputStream();
//                            int totalSize = urlConnection.getContentLength();
//                            int downloadedSize = 0;
//                            byte[] buffer = new byte[2048];
//                            int bufferLength = 0;
//                            long total = 0;
//                            InputStream input = new BufferedInputStream(
//                                    url.openStream(), 78000);
//                            while ((count = input.read(buffer)) != -1) {
//                                total += count;
//                                // writing data to file
//                                fileOutput.write(buffer, 0, count);
//                            }
//                            fileOutput.flush();
//                            input.close();
//
//                            fileOutput.close();
//                if (downloadedSize == totalSize)

                                    }
                                }

                        }
//                            Log.e("list", "os" + lists.size() + " " + stickerCount);

                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        finally {
                            urlConnection2.disconnect();
                        }
//
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


            return stickerpath;

        }

        protected void onPostExecute(String response) {
//            progressBar.setVisibility(View.INVISIBLE);
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            try {
//                Log.e("res", "is" + response);

//                Log.e("list", "os" + lists.size() + " " + stickerCount);
//                if (stickerCount == lists.size()) {
//                    StickerBook.addStickerPackExisting(stickerpack);
//                    progressDialog.dismiss();
//                }

//                addStickerPackExisting(pack);



            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
//    public void setStickers(Uri uri ,String localFile,StickerPack stickerpack){
//        lists.add(localFile);
//        stickerpack.addSticker(uri, StickerPackSearchList.this);
//    }
    public class Loading extends AsyncTask<String, Void, String> {
        private ProgressDialog dialog;
        int list,count;
        StickerPack currentPack;

        public Loading(int list,int count, StickerPack currentPack ) {
            this.list=list;
            this.count=count;
            this.currentPack=currentPack;

        }
//        final ProgressDialog progress = ProgressDialog.show(StickerPackSearchList.this,
//                "Loading......", "please wait.", true, false);
        final AlertDialog alertDialog = new AlertDialog.Builder(StickerPackSearchList.this).create();
//        CustomProgress customProgress = CustomProgress.getInstance();

// now you have the instance of CustomProgres
// for showing the ProgressBar



// for hiding the ProgressBar




        protected void onPreExecute() {
            super.onPreExecute();


            StickerPackSearchList.this.runOnUiThread(new Runnable() {
                public void run() {
//                    Log.e("on====","pre"+list+count);
                    progress = ProgressDialog.show(StickerPackSearchList.this,
                            "DownLoading......", "please wait.", true, false);
                    progress.show();
                }
            });
//            dialog.setMessage("Doing something, please wait.");
//            dialog.show();


//            customProgress.showProgress(StickerPackSearchList.this, "Loading..",false);
//            LayoutInflater layoutInflater = LayoutInflater.from(StickerPackSearchList.this);
//            View promptView = layoutInflater.inflate(R.layout.activity_sticker_pack_info, null);
//            alertDialog.setCancelable(false);
//        alertDialog.setMessage("Please, Do not close this App just Reopen WhatsApp to see all sticker inside this collection.");
//
//            alertDialog.setView(promptView);
//            alertDialog.show();


//            alertDialog.setTitle("Invalid Action");
//            alertDialog.setMessage("Please add at least 3 stickers, and let's go.");
//            alertDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
//            Log.e("do==","back");
//            try {
//                Thread.sleep(5000);
//
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            return null;
        }

        protected void onProgressUpdate(String... values) {
//            if (dialog.isShowing()) {
//                dialog.dismiss();
//            }

        }
        protected void onPostExecute(String response) {
//            Log.e("dis++++++++++++", "pre" + list + count);
            super.onPostExecute(response);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
//            customProgress.hideProgress();

//                                        customProgress.hideProgress();
//                                    alertDialog.dismiss();
//                                        progress.dismiss();






            if (response == null) {
                response = "THERE WAS AN ERROR";
            }

//            alertDialog.dismiss();

        }
    }
    private StickerPackSearchListAdapter.OnSearchButtonClickedListener onAddButtonClickedListener = new StickerPackSearchListAdapter.OnSearchButtonClickedListener() {
        @Override
        public void onAddButtonClicked(StickerSearchPack pack, int i,String packname){
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                mInterstitialAd = new InterstitialAd(StickerPackSearchList.this);
                mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
                mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());
            }
//            progress = ProgressDialog.show(StickerPackSearchList.this,
//                    "DownLoading......", "please wait.", true, false);
//            CustomProgress customProgress = CustomProgress.getInstance();
             AlertDialog alertDialog = new AlertDialog.Builder(StickerPackSearchList.this).create();
//            LayoutInflater layoutInflater = LayoutInflater.from(StickerPackSearchList.this);
//            View promptView = layoutInflater.inflate(R.layout.activity_sticker_pack_info, null);
//            alertDialog.setCancelable(false);
//            alertDialog.setMessage("Please, Do not close this App just Reopen WhatsApp to see all sticker inside this collection.");
//
//            alertDialog.setView(promptView);
//            alertDialog.show();
//            final ProgressDialog progress = ProgressDialog.show(StickerPackSearchList.this,
//                    "Loading......", "please wait.", true, false);
//            customProgress.showProgress(StickerPackSearchList.this, "Loading..",false);
            StickerPack currentPack = null;
            int stickerCount = 0;
            List<String> list = new ArrayList<String>();
            String stickerpath = null;
//            Log.e("pack", "id" + i);
//            pack.setIsDownlonload(true);

            new getBitmap2(pack,i,packname,pack.getTrayImageUri()).execute();

//                            try {
//                                URL url = new URL(API_URL);
//                                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
////                                progress.show();
//                                try {
//
//
//                                    Log.e("current", "p" + pack);
//
//                                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//                                    StringBuilder stringBuilder = new StringBuilder();
//                                    String line;
//                                    while ((line = bufferedReader.readLine()) != null) {
//                                        stringBuilder.append(line).append("\n");
//                                    }
//                                    bufferedReader.close();
//                                    String response = stringBuilder.toString();
//                                    JSONObject objMAin = new JSONObject(response);
//                                    JSONArray arr = objMAin.getJSONArray("packs");
//                                    JSONObject obj = (JSONObject) arr.get(0);
//                                    JSONArray arrpacks = obj.getJSONArray("sticker_packs");
//
//                                    JSONObject objarr = arrpacks.getJSONObject(i);
//                                    JSONArray arrStick = objarr.getJSONArray("stickers");
//                                    String id = objarr.getString("identifier");
//                                    Log.e("i", "id" + id);
//                                    currentPack = new StickerPack(pack.getIdentifier(), pack.getName(), pack.getPublisher(), pack.getTrayImageUri(), "", "", "", "", StickerPackSearchList.this);
//                                    new getBitmap2(id,currentPack).execute(arrStick);
//                                    for (int k = 0; k < arrStick.length(); k++) {
//
////                                        progress.show();
//                                                Log.e("on====","pre"+list+count);
//
////                                               progress = ProgressDialog.show(StickerPackSearchList.this,
////                                                        "DownLoading......", "please wait.", true, false);
////                                                progress.show();
//
//                                        String stickers = arrStick.getJSONObject(k).getString("image_file");
//                                        Log.e("all", "stick" + stickers + id);
//                                        URL url2 = new URL(stickers);
//                                        File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
//                                        String filename = id + stickerCount + ".png";
//                                        HttpURLConnection urlConnection2 = (HttpURLConnection) url2.openConnection();
//                                        urlConnection2.setRequestMethod("GET");
//                                        urlConnection2.setDoOutput(true);
//                                        urlConnection2.connect();
//                                        stickerCount++;
//                                        Log.i("Local filename:", "" + filename + stickerCount);
//                                        File file = new File(SDCardRoot, filename);
//                                        if (file.createNewFile()) {
//                                            file.createNewFile();
//                                        }
//                                        FileOutputStream fileOutput = new FileOutputStream(file);
//                                        int totalSize = urlConnection2.getContentLength();
//                                        int downloadedSize = 0;
//                                        byte[] buffer = new byte[2048];
//                                        int bufferLength = 0;
//                                        long total = 0;
//                                        InputStream input = new BufferedInputStream(
//                                                url2.openStream(), 10248000);
//                                        while ((count = input.read(buffer)) != -1) {
//                                            total += count;
//
//                                            fileOutput.write(buffer, 0, count);
//
////                                        mP.setProgress((int)((total*100)/totalSize));
//                                        }
//                                        fileOutput.flush();
//                                        input.close();
//                                        fileOutput.close();
////                if (downloadedSize == totalSize)
//                                        stickerpath = file.getPath();
//                                        Log.i("filepath:", " " + stickerpath);
//
//                                        list.add(stickerpath);
//                                        currentPack.addSticker(Uri.fromFile(new File(stickerpath)), StickerPackSearchList.this);
////                                        new Loading(list.size(), stickerCount,currentPack).execute();
//                                    }

//                                    Log.e("list", "os" + list.size() + " " + stickerCount);
//                                    if(list.size()==stickerCount) {
////                                        alertDialog.dismiss();
////                                        progress.dismiss();
////                                        Log.e("list", "os" + list.size() + " " + stickerCount);
//                                        StickerBook.addStickerPackExisting(currentPack);
//                                    }

//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                } catch (MalformedURLException e) {
//                                    e.printStackTrace();
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                } finally {
//                                    urlConnection.disconnect();
//                                }
//
//                            } catch (MalformedURLException e) {
//                                e.printStackTrace();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
            SearchBook.init(StickerPackSearchList.this);
            SearchBook.updatepack(i);
            ArrayList<StickerSearchPack> arr_search = DataArchiver.readsearchPackJSON(StickerPackSearchList.this);
//            Log.e("arr", "is" + arr_search.get(i).getIsDownlonload());
//
            allStickerPacksListAdapter = new StickerPackSearchListAdapter(arr_search, onAddButtonClickedListener);
            packRecyclerView.setAdapter(allStickerPacksListAdapter);
            allStickerPacksListAdapter.notifyDataSetChanged();
        };//end of onClick method
    };

//   public class DownloadBroadcastReceiver extends BroadcastReceiver {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
//                long downloadId = intent.getLongExtra(
//                        DownloadManager.EXTRA_DOWNLOAD_ID, 0);
//
//                DownloadManager myDownloadManager = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
//                Cursor c = myDownloadManager.query(new DownloadManager.Query().setFilterById(downloadId));
//                if (c.moveToFirst()) {
//                    int columnIndex = c
//                            .getColumnIndex(DownloadManager.COLUMN_STATUS);
//                    if (DownloadManager.STATUS_SUCCESSFUL == c
//                            .getInt(columnIndex)) {
//
//                        int filenameIndex = c.getColumnIndex(DownloadManager.COLUMN_TITLE);
//                        String filename = c.getString(filenameIndex);
//
//                        int filePathIndex = c.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION);
//                        String filePath = c.getString(filePathIndex);
//
//
//                        if (!filename.isEmpty()) {
//                            int dotposition = filename.lastIndexOf(".");
//                            String filename_Without_Ext = filename.substring(0, dotposition);
//                            String Ext = filename.substring(dotposition + 1, filename.length());
//                            String newFileName = filename_Without_Ext + ".change" + Ext;
//                            boolean success = new File(filePath + "/" + filename).
//                                    renameTo(new File(filePath + "/" + newFileName));
//                            Log.d("Log", "" + newFileName +success);
//                        }
//
//                    }
//                }
//            }
//        }
//    }

}



