package com.smartstickercreator.WhatsAppBasedCode;

/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */


import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.smartstickercreator.ImageManipulation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class StickerSearchPack implements Parcelable {
    Uri trayImageUri=null;
    String identifier;
    String name;
    String publisher;
    String trayImageFile;
    String publisherEmail;
    String publisherWebsite;
    String privacyPolicyWebsite;
    String licenseAgreementWebsite;
Boolean IsDownlonload=false;
    String iosAppStoreLink;
    private ArrayList<Sticker2> stickers=new ArrayList<Sticker2>();
    private long totalSize;
    String androidPlayStoreLink;
    private boolean isWhitelisted;
    private int stickersAddedIndex = 0;
    List<Uri> listuri=new ArrayList<Uri>();

    /*public StickerPack(String identifier, String name, String publisher, String trayImageFile, String publisherEmail, String publisherWebsite, String privacyPolicyWebsite, String licenseAgreementWebsite) {
        this.identifier = identifier;
        this.name = name;
        this.publisher = publisher;
        this.trayImageFile = trayImageFile;
        this.trayImageUri = Uri.parse("");
        this.publisherEmail = publisherEmail;
        this.publisherWebsite = publisherWebsite;
        this.privacyPolicyWebsite = privacyPolicyWebsite;
        this.licenseAgreementWebsite = licenseAgreementWebsite;
        this.stickers = new ArrayList<>();
    }*/

    public StickerSearchPack(String identifier, String name, String publisher, Uri trayImageUri, String publisherEmail, String publisherWebsite, String privacyPolicyWebsite, String licenseAgreementWebsite, Context context,Boolean IsDownlonload,List<Uri> list) {
        this.identifier = identifier;
        this.name = name;
        this.publisher = publisher;
        this.trayImageFile = "trayimage";
        this.trayImageUri = ImageManipulation.convertIconTrayToWebP(trayImageUri, this.identifier, "trayImage", context);
        this.publisherEmail = publisherEmail;
        this.publisherWebsite = publisherWebsite;
        this.privacyPolicyWebsite = privacyPolicyWebsite;
        this.licenseAgreementWebsite = licenseAgreementWebsite;
        this.IsDownlonload=IsDownlonload;
        this.listuri=list;
//        this.stickers = new ArrayList<>();
    }

    void setIsWhitelisted(boolean isWhitelisted) {
        this.isWhitelisted = isWhitelisted;
    }

    boolean getIsWhitelisted() {
        return isWhitelisted;
    }

    protected StickerSearchPack(Parcel in) {
        identifier = in.readString();
        name = in.readString();
        publisher = in.readString();
        trayImageFile = in.readString();
        publisherEmail = in.readString();
        publisherWebsite = in.readString();
        privacyPolicyWebsite = in.readString();
        licenseAgreementWebsite = in.readString();
        iosAppStoreLink = in.readString();
        stickers = in.createTypedArrayList(Sticker2.CREATOR);
        totalSize = in.readLong();
        androidPlayStoreLink = in.readString();
        isWhitelisted = in.readByte() != 0;
//        listuri=in.createTypedArrayList(Sticker2.CREATOR);

    }

    public static final Creator<StickerSearchPack> CREATOR = new Creator<StickerSearchPack>() {
        @Override
        public StickerSearchPack createFromParcel(Parcel in) {
            return new StickerSearchPack(in);
        }

        @Override
        public StickerSearchPack[] newArray(int size) {
            return new StickerSearchPack[size];
        }
    };

    public void addSticker(Uri uri, Context context){
        String index = String.valueOf(stickersAddedIndex);
        this.stickers.add(new Sticker2(
                index,
                uri,
                new ArrayList<String>()));

//        this.stickers.add(new Sticker2(uri, this.identifier, index, context),
//                new ArrayList<String>()));
        stickersAddedIndex++;
        Log.e("ad","index"+stickers);
    }

    public void deleteSticker(Sticker2 sticker){
        new File(sticker.getUri().getPath()).delete();
        this.stickers.remove(sticker);
    }
    public void editSticker(Sticker2 editSticker){
//        Log.e("edit","d", (Throwable) this.stickers);
//        String index = String.valueOf(stickersAddedIndex);
//        this.stickers.add(new Sticker(
//                index,
//                ImageManipulation.convertImageToWebP(uri, this.identifier, index, context),
//                new ArrayList<String>()));
//        stickersAddedIndex++;
    }



    public Sticker2 getSticker(int index){
        return this.stickers.get(index);
    }

    public Sticker2 getStickerById(int index){
        for(Sticker2 s : this.stickers){
            if(s.getImageFileName().equals(String.valueOf(index))){
                return s;
            }
        }
        return null;
    }

    public void setAndroidPlayStoreLink(String androidPlayStoreLink) {
        this.androidPlayStoreLink = androidPlayStoreLink;
    }

    public void setIosAppStoreLink(String iosAppStoreLink) {
        this.iosAppStoreLink = iosAppStoreLink;
    }
    void setStickers(ArrayList<Sticker2> stickers) {
        this.stickers = stickers;
        totalSize = 0;
        for (Sticker2 sticker : stickers) {
            totalSize += sticker.size;
        }
    }
    public ArrayList<Sticker2> getStickers() {
        return stickers;
    }

    public long getTotalSize() {
        return totalSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identifier);
        dest.writeString(name);
        dest.writeString(publisher);
        dest.writeString(trayImageFile);
        dest.writeString(publisherEmail);
        dest.writeString(publisherWebsite);
        dest.writeString(privacyPolicyWebsite);
        dest.writeString(licenseAgreementWebsite);
        dest.writeString(iosAppStoreLink);
        dest.writeTypedList(stickers);
        dest.writeLong(totalSize);
        dest.writeString(androidPlayStoreLink);
        dest.writeByte((byte) (isWhitelisted ? 1 : 0));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Uri getTrayImageUri() {
        return trayImageUri;
    }

    public Uri setTrayImageUri(Uri trayImageFile,Context context) {
        this.trayImageUri = ImageManipulation.convertIconTrayToWebP(trayImageFile, this.identifier, "trayImage", context);
        return trayImageUri;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;

    }
    public void setPublisherEmail(String publisherEmail) {
        this.publisherEmail= publisherEmail;
    }
    public void setPublisherWebsite(String PublisherWebsite) {
        this.publisherWebsite= publisherWebsite;
    }
    public void setPrivacyPolicyWebsite(String privacyPolicyWebsite) {
        this.privacyPolicyWebsite= privacyPolicyWebsite;
    }
    public void setLicenseAgreementWebsite(String licenseAgreementWebsite) {
        this.licenseAgreementWebsite= licenseAgreementWebsite;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getPublisherEmail() {
        return publisherEmail;
    }

    public String getPublisherWebsite() {
        return publisherWebsite;
    }

    public String getPrivacyPolicyWebsite() {
        return privacyPolicyWebsite;
    }

    public String getLicenseAgreementWebsite() {
        return licenseAgreementWebsite;
    }

    public void  setIsDownlonload(Boolean isDownlonload){
        this.IsDownlonload=isDownlonload;

    }
    public Boolean getIsDownlonload() {
        return IsDownlonload;
    }
    public List<Uri> getListuri(){
        return listuri;
    }

}

