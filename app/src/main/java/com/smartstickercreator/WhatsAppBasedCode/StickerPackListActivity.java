/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.smartstickercreator.WhatsAppBasedCode;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
//import android.support.multidex.MultiDex;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.soloader.SoLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.smartstickercreator.BuildConfig;
import com.smartstickercreator.DataArchiver;
import com.smartstickercreator.NewUserIntroActivity;
import com.smartstickercreator.PreLoader;
import com.smartstickercreator.R;
import com.smartstickercreator.StickerBook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import co.mobiwise.materialintro.shape.Focus;
import co.mobiwise.materialintro.shape.FocusGravity;
import co.mobiwise.materialintro.shape.ShapeType;
import co.mobiwise.materialintro.view.MaterialIntroView;

import static com.smartstickercreator.BitmapUtility.getResizedBitmap;
import static com.smartstickercreator.NewUserIntroActivity.verifyStoragePermissions;
import static com.smartstickercreator.StickerBook.addStickerPackExisting;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackDetailsActivity.EXTRA_STICKER_PACK_AUTHORITY;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackDetailsActivity.EXTRA_STICKER_PACK_ID;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackDetailsActivity.EXTRA_STICKER_PACK_NAME;


public class StickerPackListActivity extends BaseActivity implements View.OnTouchListener{
    public static final String EXTRA_STICKER_PACK_LIST_DATA = "sticker_pack_list";
    private static final int STICKER_PREVIEW_DISPLAY_LIMIT = 5;
    private static final String TAG = "StickerPackList";
    private LinearLayoutManager packLayoutManager;
    private static RecyclerView packRecyclerView;
    private static StickerPackListAdapter allStickerPacksListAdapter;
    WhiteListCheckAsyncTask whiteListCheckAsyncTask;
    List<StickerPack> stickerPackList;
    public static Context context;
    public static String newName, newCreator;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private BillingClient mBillingClient;
    LinearLayout fab,action_search,home;
    SharedPreferences pref;
    String versioncode = "1", sharedVal,path;
    public static String API_URL="http://erronak.com/dev/smartstickers/";
    SharedPreferences.Editor mEditor;
    List<StickerPack> list;
    StickerPack stickerPack;
    Boolean visible=false;
//    private static final String TAG = "Panoramio";

    private static final int IO_BUFFER_SIZE = 4 * 1024;
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sticker_pack_list);

        MobileAds.initialize(this, getString(R.string.admob_ad_id));
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest
                .Builder()
                .addTestDevice(getString(R.string.test_device))
                .build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());
//        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//

            verifyStoragePermissions(StickerPackListActivity.this);


        StickerBook.init(this);

        Fresco.initialize(this);



        context = getApplicationContext();

        SoLoader.init(this, /* native exopackage */ false);
//        home=(LinearLayout)findViewById(R.id.home);
//        home.setOnTouchListener(this);
//        home.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                v.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            }
//        });
        fab=(LinearLayout)findViewById(R.id.fabpack);
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabpack);
        fab.setOnTouchListener(this);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                visible=true;
                addNewStickerPackInInterface();
            }
        });
        action_search=(LinearLayout)findViewById(R.id.action_search);
        action_search.setOnTouchListener(this);
        action_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Intent intent = new Intent(StickerPackListActivity.this, StickerPackSearchList.class);
            intent.putExtra("downloadPack",false);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            }
        });
        SharedPreferences mSettings = context.getSharedPreferences("StickerMaker", Context.MODE_PRIVATE);
         mEditor = mSettings.edit();
//        mEditor.putString("version", "0");
        sharedVal = mSettings.getString("version", "0");
        Log.e("val", "is" + sharedVal);

        if (sharedVal.matches(versioncode)){

        } else {
//
            Log.e("not", "match" + versioncode + " " + sharedVal);
            // Instantiate the RequestQueue.
//            new RetrieveFeedTask().execute();
        }

//        mBillingClient = BillingClient.newBuilder(this).setListener(new PurchasesUpdatedListener() {
//            @Override
//            public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
//
//            }
//        }).build();
//        mBillingClient.startConnection(new BillingClientStateListener() {
//            @Override
//            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
//                if (billingResponseCode == BillingClient.BillingResponse.OK) {
//                    // The billing client is ready. You can query purchases here.
//                }
//            }
//            @Override
//            public void onBillingServiceDisconnected() {
//
//            }
//        });
        String val=getIntent().getStringExtra("val");
        if(val!=null) {
            if (val.contains("newpack")) {
                Log.e("al", "os" + val);
                List<StickerPack>  stickerPackList = StickerBook.getAllStickerPacks();
                showStickerPackList(stickerPackList);
            }
        }
        packRecyclerView = findViewById(R.id.sticker_pack_list);
        stickerPackList = StickerBook.getAllStickerPacks();

//            readContentFile(Objects.requireNonNull(getApplicationContext()));

        //getIntent().getParcelableArrayListExtra( EXTRA_STICKER_PACK_LIST_DATA);
        showStickerPackList(stickerPackList);
        Log.e("sticker", "pavks" + stickerPackList);
        if (Intent.ACTION_SEND.equals(getIntent().getAction())){
            Log.e("semd", "Action");
            Bundle extras = getIntent().getExtras();
            if (extras.containsKey(Intent.EXTRA_STREAM)) {
                Uri uri = (Uri) extras.getParcelable(Intent.EXTRA_STREAM);
                if (uri != null) {
                    DataArchiver.importZipFileToStickerPack(uri, StickerPackListActivity.this);
                }
            }
        }
        if (toShowIntro()) {
//            verifyStoragePermissions(StickerPackListActivity.this);
            startActivityForResult(new Intent(this, NewUserIntroActivity.class), 1114);
        }
//        List<StickerPack> list=getIntent().getParcelableArrayListExtra("listPack");
//        Log.e("list","ol"+list);
//        DataArchiver.writeStickerBookJSON(list, StickerPackListActivity.this);

    }

//    private synchronized void readContentFile(@NonNull Context context) {
//        try (InputStream contentsInputStream = context.getAssets().open(CONTENT_FILE_NAME)) {
//            Log.e("content", "ii" + contentsInputStream);
//            stickerPackList = ContentFileParser.parseStickerPacks(contentsInputStream);
//        } catch (IOException | IllegalStateException e) {
//            throw new RuntimeException(CONTENT_FILE_NAME + " file has some issues: " + e.getMessage(), e);
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();

        String action = getIntent().getAction();
        if (action == null) {
            Log.v("Example", "Force restart");
            Intent intent = new Intent(this, StickerPackListActivity.class);
            intent.setAction("Already created");
            startActivity(intent);
            finish();
        }

        whiteListCheckAsyncTask = new WhiteListCheckAsyncTask(this);
        //noinspection unchecked
        whiteListCheckAsyncTask.execute(stickerPackList);
    }


    @Override
    protected void onPause() {
        super.onPause();
        DataArchiver.writeStickerBookJSON(StickerBook.getAllStickerPacks(), this);
        if (whiteListCheckAsyncTask != null && !whiteListCheckAsyncTask.isCancelled()) {
            whiteListCheckAsyncTask.cancel(true);
        }
    }

    @Override
    protected void onDestroy() {
        Log.e("on","des"+StickerBook.getAllStickerPacks());
        DataArchiver.writeStickerBookJSON(StickerBook.getAllStickerPacks(), this);
        super.onDestroy();
    }


    public void showStickerPackList(List<StickerPack> stickerPackList) {
        allStickerPacksListAdapter = new StickerPackListAdapter(stickerPackList, onAddButtonClickedListener);
        packRecyclerView.setAdapter(allStickerPacksListAdapter);
        packLayoutManager = new LinearLayoutManager(this);
        packLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                packRecyclerView.getContext(),
                packLayoutManager.getOrientation()
        );
        packRecyclerView.addItemDecoration(dividerItemDecoration);
        packRecyclerView.setLayoutManager(packLayoutManager);
        packRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(this::recalculateColumnCount);
    }


    private StickerPackListAdapter.OnAddButtonClickedListener onAddButtonClickedListener = new StickerPackListAdapter.OnAddButtonClickedListener() {
        @Override
        public void onAddButtonClicked(StickerPack pack) {
            if (pack.getStickers().size() >= 3) {
                Intent intent = new Intent();
                intent.setAction("com.whatsapp.intent.action.ENABLE_STICKER_PACK");
                intent.putExtra(EXTRA_STICKER_PACK_ID, pack.identifier);
                intent.putExtra(EXTRA_STICKER_PACK_AUTHORITY, BuildConfig.CONTENT_PROVIDER_AUTHORITY);
                intent.putExtra(EXTRA_STICKER_PACK_NAME, pack.name);
                try {
                    StickerPackListActivity.this.startActivityForResult(intent, StickerPackDetailsActivity.ADD_PACK);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(StickerPackListActivity.this, R.string.error_adding_sticker_pack, Toast.LENGTH_LONG).show();
                }
            } else {
//                AlertDialog alertDialog = new AlertDialog.Builder(StickerPackListActivity.this)
//                        .setNegativeButton("Got It", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                            }
//                        })
//                        .create();
//                alertDialog.setTitle("Invalid Action");
//                alertDialog.setMessage("Please add at least 3 stickers, and let's go.");
//                alertDialog.show();
            }
        }
    };

    private void recalculateColumnCount() {
        final int previewSize = getResources().getDimensionPixelSize(R.dimen.sticker_pack_list_item_preview_image_size);
        int firstVisibleItemPosition = packLayoutManager.findFirstVisibleItemPosition();
        StickerPackListItemViewHolder viewHolder = (StickerPackListItemViewHolder) packRecyclerView.findViewHolderForAdapterPosition(firstVisibleItemPosition);
        if (viewHolder != null) {
            final int max = Math.max(viewHolder.imageRowView.getMeasuredWidth() / previewSize, 1);
            int numColumns = Math.min(STICKER_PREVIEW_DISPLAY_LIMIT, max);
            allStickerPacksListAdapter.setMaxNumberOfStickersInARow(numColumns);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == StickerPackDetailsActivity.ADD_PACK) {
            if (resultCode == Activity.RESULT_CANCELED && data != null) {
                final String validationError = data.getStringExtra("validation_error");
                if (validationError != null) {
                    if (BuildConfig.DEBUG) {
                        //validation error should be shown to developer only, not users.
                        MessageDialogFragment.newInstance(R.string.title_validation_error, validationError).show(getSupportFragmentManager(), "validation error");
                    }
                    Log.e(TAG, "Validation failed:" + validationError);
                }
            } else {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    mInterstitialAd = new InterstitialAd(this);
                    mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
                    mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());
                }
            }
        } else if (data != null && requestCode == 2319) {
            Bitmap mBitmap = null;
            Uri uri = data.getData();
            Log.e("uri", "iss" + uri);
            getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                mBitmap = MediaStore.Images.Media.getBitmap(StickerPackListActivity.this.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap imageBitmap = getResizedBitmap(mBitmap, 96, 96);
            Uri mUri = getImageUri(this, imageBitmap);
            createNewStickerPackAndOpenIt(newName, newCreator, mUri);
        } else if (requestCode == 1114) {
//            new RetrieveFeedTask().execute();
//            Toast.makeText(StickerPackListActivity.this,"Please enable permission of storage",Toast.LENGTH_LONG).show();
            makeIntroNotRunAgain();
//            AlertDialog alertDialog = new AlertDialog.Builder(StickerPackListActivity.this)
//                    .setPositiveButton("Go to Settings", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            Intent intent = new Intent();
//                            intent.setAction(Settings.ACTION
// _APPLICATION_DETAILS_SETTINGS);
//                            Uri uri = Uri.fromParts("package", "com.whatsapp", null);
//                            intent.setData(uri);
//                            startActivity(intent);
//                        }
//                    }).create();
//            alertDialog.setTitle("Please Allow permission for Storage of WhatsApp");
//            alertDialog.show();
//            new MaterialIntroView.Builder(this)
//                    .enableIcon(false)
//                    .setFocusGravity(FocusGravity.CENTER)
//                    .setFocusType(Focus.MINIMUM)
//                    .setDelayMillis(500)
//                    .enableFadeAnimation(true)
//                    .performClick(true)
//                    .setInfoText("Download Sticker Collections")
//                    .setShape(ShapeType.CIRCLE)
//                    .setTarget(findViewById(R.id.action_search))
//                    .setUsageId("intro_card") //THIS SHOULD BE UNIQUE ID
//                    .show();
//            new MaterialIntroView.Builder(this)
//                    .enableIcon(false)
//                    .setFocusGravity(FocusGravity.CENTER)
//                    .setFocusType(Focus.MINIMUM)
//                    .setDelayMillis(100)
//                    .enableFadeAnimation(true)
//                    .performClick(true)
//                    .setInfoText("To add new sticker Collections, click here.")
//                    .setShape(ShapeType.CIRCLE)
//                    .setTarget(findViewById(R.id.fabpack))
//                    .setUsageId("intro_card") //THIS SHOULD BE UNIQUE ID
//                    .show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.action_add) {
////
//            Bitmap bitmap=null;
//           File bitmapFile = new File(Environment.getExternalStorageDirectory() + "/" +"IMG_20181206_110550.jpg");
//            bitmap = BitmapFactory.decodeFile(String.valueOf(bitmapFile));
////
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//            Log.e("im","j"+bitmap);
//            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "img", null);
//            Log.e("fg","d"+Uri.parse(path));
//
//            StickerPack stickerPack=new StickerPack("java","java","nidhi",Uri.parse(path),"","","","",StickerPackListActivity.this);
//            stickerPack.addSticker(Uri.parse(path),StickerPackListActivity.this);
//            stickerPack.addSticker(Uri.parse(path),StickerPackListActivity.this);
//            stickerPack.addSticker(Uri.parse(path),StickerPackListActivity.this);
//
////            dirChecker(context.getFilesDir()+"/"+StickerBookId);
////
////            String path = context.getFilesDir()+"/"+StickerBookId+"/"+StickerBookId+"-"+StickerId+".webp";
////                        stickerPack.addSticker
//            Intent intent = new Intent();
////            String authority = BuildConfig.CONTENT_PROVIDER_AUTHORITY;
//            intent.setAction("com.whatsapp.intent.action.ENABLE_STICKER_PACK");
////            Log.w("IS IT A NEW IDENTIFIER?", sp.getIdentifier());
//            intent.putExtra(EXTRA_STICKER_PACK_ID, stickerPack.identifier);
//            intent.putExtra(EXTRA_STICKER_PACK_AUTHORITY, BuildConfig.CONTENT_PROVIDER_AUTHORITY);
//            intent.putExtra(EXTRA_STICKER_PACK_NAME, stickerPack.name);
//            try {
//                startActivityForResult(intent, 200);
//            } catch (ActivityNotFoundException e) {
//                Toast.makeText(this, R.string.error_adding_sticker_pack, Toast.LENGTH_LONG).show();
//            }
////            addNewStickerPackInInterface();
//            return true;
//        }
        if (item.getItemId() == R.id.action_info) {

            Intent intent = new Intent(StickerPackListActivity.this, AboutAppActivity.class);
            startActivity(intent);
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
//            LayoutInflater inflater = this.getLayoutInflater();
//            View dialogView = inflater.inflate(R.layout.about_layout, null);

//            dialogView.findViewById(R.id.redditlogo).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com/u/idoideas"));
//                    startActivity(browserIntent);
//                }
//            });
//
//            dialogView.findViewById(R.id.twitterlogo).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/idoideas"));
//                    startActivity(browserIntent);
//                }
//            });

//            dialogView.findViewById(R.id.githublogo).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.github.com/idoideas"));
//                    startActivity(browserIntent);
//                }
//            });

//            dialogBuilder.setView(dialogView);
//            AlertDialog alertDialog = dialogBuilder.create();
//            alertDialog.show();
        }
//        else if(item.getItemId() == R.id.action_search){
//
//            Intent intent = new Intent(StickerPackListActivity.this, StickerPackSearchList.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//        }
//        else if (item.getItemId() == R.id.action_donate){
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Thank you for donation!");
//            builder.setMessage("We appreciate your support in great apps and open-source projects.\n\nHow much would you like to donate?");
//            builder.setPositiveButton("A Sandwich - 5$",
//                    new DialogInterface.OnClickListener()
//                    {
//                        public void onClick(DialogInterface dialog, int id)
//                        {
//                            dialog.cancel();
//                            startInAppPurchase("5_dollar_donation");
//                        }
//                    });
//
//            builder.setNeutralButton("A Piece of Gum - 1$",
//                    new DialogInterface.OnClickListener()
//                    {
//                        public void onClick(DialogInterface dialog, int id)
//                        {
//                            dialog.cancel();
//                            startInAppPurchase("1_dollar_donation");
//                        }
//                    });
//
//            builder.setNegativeButton("A Coffee - 3$",
//                    new DialogInterface.OnClickListener()
//                    {
//                        public void onClick(DialogInterface dialog, int id)
//                        {
//                            dialog.cancel();
//                            startInAppPurchase("3_dollar_donation");
//                        }
//                    });
//            builder.create().show();
//        }
        return super.onOptionsItemSelected(item);
    }

//    public static Bitmap getBitmapFromURL(String src) {
//        try {
//            URL url = new URL(src);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Bitmap myBitmap = BitmapFactory.decodeStream(input);
//            return myBitmap;
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    public static Bitmap loadBitmap(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;

        try {
            in = new BufferedInputStream(new URL(url).openStream(), IO_BUFFER_SIZE);

            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            copy(in, out);
            out.flush();

            final byte[] data = dataStream.toByteArray();
            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        } catch (IOException e) {
            Log.e(TAG, "Could not load Bitmap from: " + url);
        } finally {
            closeStream(in);
            closeStream(out);
        }

        return bitmap;
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(TAG, "Could not close stream", e);
            }
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId()==R.id.action_search){
            v.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }else if(v.getId()==R.id.fabpack){
            v.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
//        else if(v.getId()==R.id.home){
//            v.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//        }
        else{
            v.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
        return false;
    }

    static class WhiteListCheckAsyncTask extends AsyncTask<List<StickerPack>, Void, List<StickerPack>> {
        private final WeakReference<StickerPackListActivity> stickerPackListActivityWeakReference;

        WhiteListCheckAsyncTask(StickerPackListActivity stickerPackListActivity) {
            this.stickerPackListActivityWeakReference = new WeakReference<>(stickerPackListActivity);
        }

        @SafeVarargs
        @Override
        protected final List<StickerPack> doInBackground(List<StickerPack>... lists) {
            List<StickerPack> stickerPackList = lists[0];
            final StickerPackListActivity stickerPackListActivity = stickerPackListActivityWeakReference.get();
            if (stickerPackListActivity == null) {
                return stickerPackList;
            }
            for (StickerPack stickerPack : stickerPackList) {
                stickerPack.setIsWhitelisted(WhitelistCheck.isWhitelisted(stickerPackListActivity, stickerPack.identifier));
            }
            return stickerPackList;
        }

        @Override
        protected void onPostExecute(List<StickerPack> stickerPackList) {
            final StickerPackListActivity stickerPackListActivity = stickerPackListActivityWeakReference.get();
            if (stickerPackListActivity != null) {
                stickerPackListActivity.allStickerPacksListAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    AlertDialog alertDialog = new AlertDialog.Builder(this)
                            .setPositiveButton("Let's Go", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    verifyStoragePermissions(StickerPackListActivity.this);
                                }
                            })
                            .create();
                    alertDialog.setTitle("Notice!");
                    alertDialog.setMessage("We've recognized you denied the storage access permission for this app."
                            + "\n\nIn order for this app to work, storage access is required.");
                    alertDialog.show();
                }
                break;
        }
    }

    private void addNewStickerPackInInterface() {
        pref = getSharedPreferences("init", Context.MODE_PRIVATE);

        String json = pref.getString("creator", null);

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Create New Collection");

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        final EditText nameBox = new EditText(this);

        nameBox.requestFocus();
        nameBox.setInputType(0);
        nameBox.setAllCaps(true);
        nameBox.setLines(1);
        LinearLayout.LayoutParams buttonLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonLayoutParams.setMargins(50, 0, 50, 10);
        nameBox.setLayoutParams(buttonLayoutParams);
        nameBox.setHint("Collection Name");
        nameBox.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);
        layout.addView(nameBox);

        int val=layout.getVisibility();
        Log.e("val","is"+val);
//        if(val==1){
//            visible=true;
//        }
//        final EditText creatorBox = new EditText(this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            creatorBox.setAutofillHints("name");
//        }
//        creatorBox.setLines(1);
//        creatorBox.setLayoutParams(buttonLayoutParams);
//        creatorBox.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE);
//        creatorBox.setText(json);
//        creatorBox.setHint("Creator");
//        layout.addView(creatorBox);

        dialog.setView(layout);

        dialog.setPositiveButton("OK", null);

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                visible=false;
                dialog.cancel();
            }
        });

        final AlertDialog ad = dialog.create();

        ad.show();
//       this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        if(visible) {
            ad.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        Button b = ad.getButton(AlertDialog.BUTTON_POSITIVE);
        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(nameBox.getText())) {
                    nameBox.setError("Package name is required!");
                }

//                if(TextUtils.isEmpty(creatorBox.getText())){
//                    creatorBox.setError("Creator is required!");
//                }


                if (!TextUtils.isEmpty(nameBox.getText())) {
                    ad.dismiss();
                    int permission = ActivityCompat.checkSelfPermission(StickerPackListActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    if (permission != PackageManager.PERMISSION_GRANTED) {
                        verifyStoragePermissions(StickerPackListActivity.this);
                    }
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                            R.drawable.stickermakerlogo);

                    Uri uri = getImageUri(StickerPackListActivity.this, bitmap);
                    newName = String.valueOf(nameBox.getText());
                    newCreator = json;
//                    openFileTray(newName, newCreator);
//                    createNewStickerPackAndOpenIt(newName, newCreator, uri);
                    createDialogForPickingIconImage(nameBox, json);
                }
            }
        });

//        creatorBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    b.performClick();
//                }
//                return false;
//            }
//        });
    }

    public static Uri getImageUri(Context context, Bitmap inImage) {
//        getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        Log.e("im","j"+inImage);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
//        Log.e("fg","d"+path);
        return Uri.parse(path);
    }

    private void createDialogForPickingIconImage(EditText nameBox, String creatorBox) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Pick your collection's icon image");
        builder.setMessage("Now you will pick the new sticker collection's icon image.")
                .setCancelable(false)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                                R.drawable.stickermakerlogo);
                        Bitmap imageBitmap = getResizedBitmap(bitmap, 96, 96);
                        Uri uri = getImageUri(StickerPackListActivity.this, imageBitmap);
                        Log.e("on", "Click" + uri + "  " + newName + newCreator);
//                getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);

                        createNewStickerPackAndOpenIt(newName, newCreator, uri);
                    }
                })
                .setPositiveButton("Let's go", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        openFileTray(newName, newCreator);
//                        openFileTray(nameBox.getText().toString(), creatorBox);
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void createNewStickerPackAndOpenIt(String name, String creator, Uri trayImage) {
        String newId = UUID.randomUUID().toString();
        StickerPack sp = new StickerPack(
                newId,
                name,
                creator,
                trayImage,
                "",
                "",
                "",
                "",
                this);
        addStickerPackExisting(sp);

        Intent intent = new Intent(this, StickerPackDetailsActivity.class);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_SHOW_UP_BUTTON, true);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_DATA, newId);
        intent.putExtra("isNewlyCreated", true);
        this.startActivity(intent);
    }

    private void openFileTray(String name, String creator) {
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        i.setType("image/*");
        newName = name;
        newCreator = creator;
        startActivityForResult(i, 2319);
    }

    private void makeIntroNotRunAgain() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean previouslyStarted = prefs.getBoolean("isAlreadyShown", false);
        if (!previouslyStarted) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean("isAlreadyShown", false);
            edit.commit();
        }
    }

    private boolean toShowIntro() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        return prefs.getBoolean("isAlreadyShown", true);
    }

    private void startInAppPurchase(String sku) {
        mBillingClient.consumeAsync("", new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(int responseCode, String purchaseToken) {

            }
        });
        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSku(sku)
                .setType(BillingClient.SkuType.INAPP)
                .build();
        mBillingClient.launchBillingFlow(StickerPackListActivity.this, flowParams);
        Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
        ConsumeResponseListener listener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(@BillingClient.BillingResponse int responseCode, String outToken) {
                if (responseCode == BillingClient.BillingResponse.OK) {
                    Toast.makeText(getApplicationContext(), "Thank you so much for your donation!", Toast.LENGTH_LONG).show();
                }
            }
        };
        if (purchasesResult != null) {
            if (purchasesResult.getPurchasesList() != null) {
                if (purchasesResult.getPurchasesList().size() > 0) {
                    for (int i = 0; i < purchasesResult.getPurchasesList().size(); i++) {
                        mBillingClient.consumeAsync(purchasesResult.getPurchasesList().get(i).getPurchaseToken(), listener);
                    }
                }
            }
        }
    }


   public class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;
       private String version;


       protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
//            String email = emailText.getText().toString();
            // Do some validation here

            try {
                URL url = new URL(API_URL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
//            progressBar.setVisibility(View.GONE);
//            Log.i("INFO", response);
            try {
                JSONObject objMAin=new JSONObject(response);
                version=objMAin.getString("version");
                list=new ArrayList<StickerPack>();
                Log.e("ver","is"+version);
                JSONArray arr=objMAin.getJSONArray("packs");
                JSONObject obj= (JSONObject) arr.get(0);
                JSONArray arr2 =obj.getJSONArray("sticker_packs");
//                Log.e("arr","packs"+arr2);
                for(int i=0;i<arr2.length();i++){
                    JSONObject objpack=arr2.getJSONObject(i);

                        Log.e("obj","pack"+objpack);
                        String urlMain = objpack.getString("tray_image_file");
                        new getBitmap1(urlMain,objpack,arr2.length()).execute();

//                        JSONArray jsonArrayarr = objpack.getJSONArray("stickers");
////                        Log.e("s", "p" + jsonArrayarr);
////                        StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), contentUri, "", "", "", "", StickerPackListActivity.this);
//                        for (int j = 0; j < jsonArrayarr.length(); j++) {
//                            String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
////
////                            Uri stickUri = Uri.parse(stickers);
////                                    new getBitmap1(stickers,).execute();
////                            Uri stickUri =Uri.parse(path);
////                            Log.e("S", "p" + path);
////                            stickerPack.addSticker(stickUri,StickerPackListActivity.this);
//                        }

//                        list.add(stickerPack);
                    }


//                Log.e("list","of"+list);
//                DataArchiver.writeStickerBookJSON(list, StickerPackListActivity.this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mEditor.putString("version",version);
//            responseView.setText(response);
        }

    }

    public class getBitmap1 extends AsyncTask<Void, Void, String> {

        private Exception exception;
        private String version;

        String url1="";
        JSONObject objpack;
        int count=0,total;

        public getBitmap1(String src,JSONObject obj,int total) {
            url1=src;
            objpack=obj;
            total=total;
        }

        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
//            String url1 = String.valueOf(urls[0]);
            // Do some validation here

            try {
                URL url;
                url = new URL(url1);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
//                getBitmapFromURL(myBitmap);
                String path = MediaStore.Images.Media.insertImage(StickerPackListActivity.this.getContentResolver(), myBitmap, "sticker", null);

                 return path;
            } catch (IOException e) {
                // Log exception
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            try {

                Log.e("im","j"+Uri.parse(response));
                stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), Uri.parse(response), "", "", "", "", StickerPackListActivity.this);
                JSONArray jsonArrayarr = objpack.getJSONArray("stickers");
//                        Log.e("s", "p" + jsonArrayarr);
//                        StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), contentUri, "", "", "", "", StickerPackListActivity.this);
                for (int j = 0; j < jsonArrayarr.length(); j++) {
                    String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
//
//                   Uri stickUri = Uri.parse(stickers);
                    new getBitmap2(stickers,objpack.getString("identifier")).execute();
//                            Uri stickUri =Uri.parse(path);
//                            Log.e("S", "p" + path);

                }
                list.add(stickerPack);
                Log.e("list","of"+list);
                DataArchiver.writeStickerBookJSON(list, StickerPackListActivity.this);
                //count++;
//               compare(total);
                //  path=response;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        public void compare(int total){
            Log.e("tot","u"+total);
            if(count==total){

            }

        }
    }
    public class getBitmap2 extends AsyncTask<Void, Void, String> {

        private Exception exception;
        private String version;

        String url1="",identifier;
        JSONObject objpack;
        int count=0,total;
//        StickerPack pack;

        public getBitmap2(String src,String identifier) {
            url1=src;
            identifier=identifier;
        }

        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
//            String url1 = String.valueOf(urls[0]);
            // Do some validation here

            try {
                URL url;
                url = new URL(url1);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
//                getBitmapFromURL(myBitmap);
                String path = MediaStore.Images.Media.insertImage(StickerPackListActivity.this.getContentResolver(), myBitmap, "sticker", null);

                return path;
            } catch (IOException e) {
                // Log exception
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            try {
//                stickerPack=StickerBook.getStickerPackById(identifier);
//                StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), Uri.parse(response), "", "", "", "", StickerPackListActivity.this);
                stickerPack.addSticker(Uri.parse(response),StickerPackListActivity.this);
                Log.e("imr","j"+Uri.parse(response)+stickerPack);
                //  path=response;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

}
