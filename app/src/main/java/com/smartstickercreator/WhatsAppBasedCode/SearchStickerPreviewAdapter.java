/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.smartstickercreator.WhatsAppBasedCode;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.smartstickercreator.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SearchStickerPreviewAdapter extends RecyclerView.Adapter<SearchStickerPreviewViewHolder> {

    @NonNull
    private StickerSearchPack stickerPack;
    public List<Uri> list=new ArrayList<Uri>();
    private final int cellSize;
    private int cellLimit;
    private int cellPadding;
    private final int errorResource;
    private final Context activity;
    private final LayoutInflater layoutInflater;
    int imageHeight,imageWidth;
    View itemView;
    ImageView imageView;
    SearchStickerPreviewAdapter(final Context activity,
                          @NonNull final LayoutInflater layoutInflater,
                          final int errorResource,
                          final int cellSize,
                          final int cellPadding,
                          @NonNull final List<Uri> list) {
        this.cellSize = cellSize;
        this.cellPadding = cellPadding;
        this.cellLimit = 0;
        this.layoutInflater = layoutInflater;
        this.errorResource = errorResource;
        this.list = list;
        this.activity=activity;
    }

    @NonNull
    @Override
    public SearchStickerPreviewViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
         itemView = layoutInflater.inflate(R.layout.search_sticker_image, viewGroup, false);
//         imageView=itemView.findViewById(R.id.search_sticker_preview);

        SearchStickerPreviewViewHolder vh = new SearchStickerPreviewViewHolder(itemView);
//        Sticker thisSticker = stickerPack.getSticker(i);
        Log.e("this","p"+list.get(i));
        DisplayMetrics dis=new DisplayMetrics();
//        activity.getWindowManager().getDefaultDisplay().getMetrics(dis);
        int original_we =dis.widthPixels;
        int original_he=dis.heightPixels;
//        vh.frme.setMinimumWidth(Integer.parseInt(String.valueOf(original_we/3-R.dimen.preview_side_margin-R.dimen.preview_side_margin)));
//        getImageWidthAndHeight(thisSticker.getUri());
//         int ratio = Math.min(original_we / imageWidth, original_he / imageHeight);
//        int new_he=(original_we*imageHeight/imageWidth);
//        Log.e("size","is"+imageHeight +"  "+imageHeight +cellSize);
//        ViewGroup.LayoutParams layoutParams = vh.stickerPreviewView.getLayoutParams();
//        layoutParams.height =cellSize;
//        layoutParams.width =cellSize ;
//        vh.stickerPreviewView.setLayoutParams(layoutParams);
//        vh.stickerPreviewView.setPadding(5,5,5, 5);

//        for (int j = 0; j <list.size(); j++) {
//            final ImageView rowImage = (ImageView) LayoutInflater.from(activity).inflate(R.layout.sticker_down_list_item_image,R.layout.search_packs_list_item, false);
//
////            final ImageView rowImage = (ImageView) LayoutInflater.from(activity).inflate(R.layout.sticker_down_list_item_image, itemView, false);
////            rowImage.setImageURI(Uri.parse("https://www.smashbros.com/wiiu-3ds/images/character/toon_link/screen-7.jpg"));
//            Glide.with(activity).load(list.get(j)).asBitmap().error(R.drawable.githublogo).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(rowImage);
////        Picasso.with(context).load("https://www.smashbros.com/wiiu-3ds/images/character/toon_link/screen-7.jpg").into(rowImage);
//// rowImage.setImageURI(Uri.parse("https://firebasestorage.googleapis.com/v0/b/smart-stickers.appspot.com/o/Merry%20Christmas%2Fnm16.webp?alt=media&token=99af9886-36fe-4f92-a824-8ebb1e3f2d57"));
//
//        }

        return vh;
    }
//    private void getImageWidthAndHeight(Uri uri){
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
//        imageHeight = options.outHeight;
//        imageWidth = options.outWidth;
//
//    }
    @Override
    public void onBindViewHolder(@NonNull final SearchStickerPreviewViewHolder stickerPreviewViewHolder, final int i) {
//        list=stickerPack.getListuri();


//        Sticker thisSticker = stickerPack.getSticker(i);
        Log.e("list","of"+list.get(i));
//        Context thisContext = stickerPreviewViewHolder.stickerPreviewView.getContext();
//        stickerPreviewViewHolder.stickerPreviewView.setImageResource(errorResource);
        stickerPreviewViewHolder.search_sticker_preview.setImageURI(list.get(i));
//        Glide.with(this.activity).load(thisSticker).asBitmap().error(R.drawable.githublogo).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(stickerPreviewViewHolder.stickerPreviewView);
//        Picasso.with(activity).load(thisSticker).into(stickerPreviewViewHolder.stickerPreviewView);
//        stickerPreviewViewHolder.stickerPreviewView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                ImageView image = new ImageView(thisContext);
////                image.setImageURI(thisSticker.getUri());
////                Log.e("e","i"+thisSticker.getUri());
////
//
//
//
//            }
//        });
    }

//    @Override
    public int getItemCount() {
        int numberOfPreviewImagesInPack;
        numberOfPreviewImagesInPack = list.size();
        if (cellLimit > 0) {
            return Math.min(numberOfPreviewImagesInPack, cellLimit);
        }
        return numberOfPreviewImagesInPack;
    }
}
