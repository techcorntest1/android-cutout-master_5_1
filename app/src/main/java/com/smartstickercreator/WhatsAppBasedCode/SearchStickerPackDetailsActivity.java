package com.smartstickercreator.WhatsAppBasedCode;

/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.smartstickercreator.BuildConfig;
import com.smartstickercreator.R;
import com.smartstickercreator.SearchBook;
import com.smartstickercreator.StickerBook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.smartstickercreator.SearchBook.getStickerPackById;
import static com.smartstickercreator.SearchBook.getStickerPackByIndex;
import static com.smartstickercreator.StickerBook.getAllStickerPacks;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackListActivity.API_URL;
//import static com.smartstickercreator.WhatsAppBasedCode.StickerPackSearchList.lists;

public class SearchStickerPackDetailsActivity extends BaseActivity {

    /**
     * Do not change below values of below 3 lines as this is also used by WhatsApp
     */
    public static final String EXTRA_STICKER_PACK_ID = "sticker_pack_id";
    public static final String EXTRA_STICKER_PACK_AUTHORITY = "sticker_pack_authority";
    public static final String EXTRA_STICKER_PACK_NAME = "sticker_pack_name";

    public static final int ADD_PACK = 200;

    public static final String EXTRA_SHOW_UP_BUTTON = "show_up_button";
    public static final String EXTRA_STICKER_PACK_DATA = "sticker_pack";
    private static final String TAG = "StickerPackDetails";

    private RecyclerView recyclerView;
    private GridLayoutManager layoutManager;
    RecyclerView.LayoutManager recyclerViewLayoutManager;
    private SearchStickerPreviewAdapter stickerPreviewAdapter;
    private int numColumns;
    private View addButton;
    private View alreadyAddedText;
    private StickerSearchPack searchPack;
    private View divider;
//    private WhiteListCheckAsyncTask whiteListCheckAsyncTask;
    private FrameLayout shareButton,detailFrame;
    //    private FrameLayout deleteButton;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private ImageView image;
    FloatingActionButton deleteButton;
    public String PackId,packNm,trayimage;
    int index;
    Boolean isdownload;
    Uri  uri;
//    List<Sticker2> list=new ArrayList<Sticker2>();
    List<Uri> list=new ArrayList<Uri>();
    FirebaseStorage storage;
    public static ProgressDialog progressDialog;
     List<String> lists = new ArrayList<String>();
    ArrayList<StickerSearchPack> arrStickerSearchBook;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_sticker_pack_details);
        FirebaseApp.initializeApp(SearchStickerPackDetailsActivity.this);
        storage = FirebaseStorage.getInstance("gs://smart-stickers.appspot.com");
        progressDialog = new ProgressDialog(SearchStickerPackDetailsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.setTitle("Downloading");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        boolean showUpButton = getIntent().getBooleanExtra(EXTRA_SHOW_UP_BUTTON, false);
//        mons
//        stickerSearchPack = StickerBook.getStickerPackById(getIntent().getStringExtra(EXTRA_STICKER_PACK_DATA));
        TextView packNameTextView = findViewById(R.id.search_pack_name);
//        TextView packPublisherTextView = findViewById(R.id.author);
        ImageView packTrayIcon = findViewById(R.id.search_tray_image);
//        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar2);
//        setSupportActionBar(myToolbar);
        detailFrame=findViewById(R.id.search_detailFrame);
//        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(myToolbar);
//        addButton =findViewById(R.id.add_to_whatsapp_button);
//        shareButton = findViewById(R.id.share_pack_button);
        deleteButton = findViewById(R.id.search_downloadimage);
        image=findViewById(R.id.search_image);
        alreadyAddedText = findViewById(R.id.already_added_text);
//        int cp=calculateNoOfColumns(SearchStickerPackDetailsActivity.this);
        Intent intent=getIntent();
        PackId=intent.getStringExtra("Packid");
        packNm=intent.getStringExtra("name");
        isdownload=getStickerPackById(PackId).getIsDownlonload();
        uri=intent.getParcelableExtra("trayimage");
        Log.e("i","trau"+uri);
        list=intent.getParcelableArrayListExtra("list");
        index=intent.getIntExtra("index",0);
         searchPack=(StickerSearchPack) intent.getParcelableExtra("searchPack");
        Log.e("pack","2"+getStickerPackById(PackId).getIsDownlonload());
        //            for set downloaded Button value

        ArrayList<StickerPack> arrlist=getAllStickerPacks();
        ArrayList<StickerSearchPack> arrayList=new ArrayList<StickerSearchPack>();
        arrStickerSearchBook = SearchBook.checkIfPacksAreNull();
        arrayList=arrStickerSearchBook;
        ArrayList<StickerPack> len2=arrlist;
        int lensz=arrayList.size();
        for (int i = 0; i < arrayList.size(); i++) {

            Log.e("is","down"+arrayList.get(i).getIsDownlonload());
            arrayList.get(i).setIsDownlonload(false);
////                        StickerSearchPack searchlistdown=SearchBook.getStickerPackById(arrayList.get(i).getIdentifier());
////                        Log.e("main","o"+searchlistdown);
////                        int pos=SearchBook.getStickerPackByPos(arrayList.get(i).getIdentifier());
////                        SearchBook.downloadpack(pos,false);
//
        }

        for (int i = 0; i < len2.size(); i++) {

            for (int j = 0; j < lensz; j++) {

                if (len2.get(i).getIdentifier().contentEquals(arrayList.get(j).getIdentifier())){

//                                    SearchBook.downloadpack(j,true);
//                                arrayList.remove(j);
//                                lensz=arrayList.size();

//                    list=arrayList.get(j).getIdentifier();
//                                if(!arrayList.get(j).getIdentifier().equalsIgnoreCase(list)){
                                    Log.e("li-----","i"+arrayList.get(j).getIdentifier());
                    SearchBook.downloadpack(j,true);

//                                }

                }
                else{
//                                Log.e("li","i"+len2.get(i).getIdentifier());
//                                SearchBook.downloadpack(j,false);
                }
            }
        }

        if(isdownload){

            deleteButton.setVisibility(View.INVISIBLE);
        }else{

            deleteButton.setVisibility(View.VISIBLE);
        }

        Log.e("i99999","colssss"+searchPack.getTrayImageUri());
        layoutManager = new GridLayoutManager(SearchStickerPackDetailsActivity.this,3);
        recyclerViewLayoutManager=new GridLayoutManager(SearchStickerPackDetailsActivity.this,3);
        recyclerView = findViewById(R.id.search_sticker_list);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
//        layoutManager.setOrientation(0);
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(pageLayoutListener);
        recyclerView.addOnScrollListener(dividerScrollListener);
        divider = findViewById(R.id.search_divider);
        if (stickerPreviewAdapter == null) {
            stickerPreviewAdapter = new SearchStickerPreviewAdapter(SearchStickerPackDetailsActivity.this,getLayoutInflater(), R.drawable.sticker_error, 205, 0, list);
//            Log.e("pack","added"+stickerPreviewAdapter.getItemCount());
            recyclerView.setAdapter(stickerPreviewAdapter);
//            stickerPreviewAdapter.notifyDataSetChanged();

        }
        if(uri!=null) {
//            Sticker thisSticker= stickerPack.getStickerById(0);
//            ArrayList<StickerPack> arr=StickerBook.checkIfPacksAreNull();
//          List<Sticker> list=stickerPack.getStickers();
//            Log.e("stickers","k"+list.get(0));

            packTrayIcon.setImageURI(uri);
//
        }
//        else{
//            Log.e("bitmap","uri null");
//            Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
//                    R.drawable.stickermakerlogo);
//            packTrayIcon.setImageBitmap(bitmap);
//
//        }
        packNameTextView.setText(packNm);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Intent intent = new Intent(SearchStickerPackDetailsActivity.this,StickerPackListActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
////                intent.setType(Intent.ACTION_MAIN);
//                startActivity(intent);
//           StickerPackListActivity.class));
//                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//// This uses android:parentActivityName and
//// android.support.PARENT_ACTIVITY meta-data by default
//                stackBuilder.addNextIntentWithParentStack(StickerPackListActivity);
//                PendingIntent pendingIntent = stackBuilder
//                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                SearchStickerPackDetailsActivity.this.finish();

//                Intent intent=new Intent(StickerPackDetailsActivity.this,StickerPackListActivity.class);
//                startActivity(intent);
            }
        });

//

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                    mInterstitialAd = new InterstitialAd(SearchStickerPackDetailsActivity.this);
//                    mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
//                    mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());

                Bundle bundle=new Bundle();
                bundle.putParcelable("Pack",searchPack);
                bundle.putParcelable("uri",uri);
                Intent intent = new Intent(SearchStickerPackDetailsActivity.this,StickerPackSearchList.class);
                 intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("downloadPack",true);
                intent.putExtra("pack",bundle);
                intent.putExtra("index",index);
                intent.putExtra("packNm",packNm);
                 startActivity(intent);
//               new getBitmap2(searchPack,index,packNm,uri).execute();
//                AlertDialog alertDialog = new AlertDialog.Builder(SearchStickerPackDetailsActivity.this)
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                            }
//                        })
//                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                                StickerBook.deleteStickerPackById(searchPack.getIdentifier());
//                                finish();
//                                Intent intent = new Intent(SearchStickerPackDetailsActivity.this, StickerPackListActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                Toast.makeText(SearchStickerPackDetailsActivity.this, "Sticker collection deleted", Toast.LENGTH_SHORT).show();
//                            }
//                        }).create();
//                alertDialog.setTitle("Are you sure to Delete sticker collection?");
//                alertDialog.setMessage("Deleting this collection will also remove it from your WhatsApp app.");
//                alertDialog.show();
            }
        });
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(showUpButton);
//            getSupportActionBar().setTitle(showUpButton ? R.string.title_activity_sticker_pack_details_multiple_pack : R.string.title_activity_sticker_pack_details_single_pack);
//            getSupportActionBar().setTitle(stickerPack.name);
//            setSupportActionBar(toolBar);
//        }

    }
//    public static int calculateNoOfColumns(Context context) {
//        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
//        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
//        int noOfColumns = (int) (dpWidth / 180);
//        return noOfColumns;
//    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
//        Intent intent = new Intent(SearchStickerPackDetailsActivity.this,StickerPackListActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.toolbar, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.action_info && stickerPack != null) {
//            AlertDialog alertDialog = new AlertDialog.Builder(StickerPackDetailsActivity.this)
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                            }
//                        })
//                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                                StickerBook.deleteStickerPackById(stickerPack.getIdentifier());
//                                finish();
//                                Intent intent = new Intent(StickerPackDetailsActivity.this, StickerPackListActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                Toast.makeText(StickerPackDetailsActivity.this, "Sticker Pack deleted", Toast.LENGTH_SHORT).show();
//                            }
//                        }).create();
//                alertDialog.setTitle("Are you sure?");
//                alertDialog.setMessage("Deleting this package will also remove it from your WhatsApp app.");
//                alertDialog.show();
////
////            final String publisherWebsite = stickerPack.publisherWebsite;
////            final String publisherEmail = stickerPack.publisherEmail;
////            final String privacyPolicyWebsite = stickerPack.privacyPolicyWebsite;
////            Uri trayIconUri = stickerPack.getTrayImageUri();
////            launchInfoActivity(publisherWebsite, publisherEmail, privacyPolicyWebsite, trayIconUri.toString());
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_PACK) {
            if (resultCode == Activity.RESULT_CANCELED && data != null) {
                final String validationError = data.getStringExtra("validation_error");
                if (validationError != null) {
                    if (BuildConfig.DEBUG) {
                        //validation error should be shown to developer only, not users.
                        MessageDialogFragment.newInstance(R.string.title_validation_error, validationError).show(getSupportFragmentManager(), "validation error");
                    }
                    Log.e(TAG, "Validation failed:" + validationError);
                }
            } else {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    mInterstitialAd = new InterstitialAd(this);
                    mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
                    mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());
                }
            }
        } else if(requestCode == 3000){
            if(data!=null){
                if(data.getClipData()!=null){
                    ClipData clipData = data.getClipData();
                    for(int i = 0; i < clipData.getItemCount(); i++)
                    {
                        ClipData.Item path = clipData.getItemAt(i);
                        Uri uri = path.getUri();
                        getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                        setImg(uri);
//                        setContentView(new CropView(PreviewSticker.this,bmp));
//
//
//
// Intent in=new Intent(StickerPackDetailsActivity.this,EditStickersActivity.class);
////                in.putExtra("im",uri);
//                startActivity(in);
                        searchPack.addSticker(uri, this);
                    }
                } else {
                    Uri uri = data.getData();
                    getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    Intent in=new Intent(StickerPackDetailsActivity.this,EditStickersActivity.class);
////                    in.putExtra("im",uri);
//                    startActivity(in);
                    searchPack.addSticker(uri, this);
//                    setImg(uri);
//                    setContentView(new CropView(PreviewSticker.this,bmp));
                }
                finish();
                startActivity(getIntent());
            }
        }
    }

    private final ViewTreeObserver.OnGlobalLayoutListener pageLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            setNumColumns(recyclerView.getWidth() / recyclerView.getContext().getResources().getDimensionPixelSize(R.dimen.sticker_pack_details_image_size));
        }
    };

    private void setNumColumns(int numColumns) {
        if (this.numColumns != numColumns) {
            layoutManager.setSpanCount(numColumns);
            this.numColumns = numColumns;
            if (stickerPreviewAdapter != null) {
                stickerPreviewAdapter.notifyDataSetChanged();
            }
        }
    }

    private final RecyclerView.OnScrollListener dividerScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull final RecyclerView recyclerView, final int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            updateDivider(recyclerView);
        }

        @Override
        public void onScrolled(@NonNull final RecyclerView recyclerView, final int dx, final int dy) {
            super.onScrolled(recyclerView, dx, dy);
            updateDivider(recyclerView);
        }

        private void updateDivider(RecyclerView recyclerView) {
            boolean showDivider = recyclerView.computeVerticalScrollOffset() > 0;
            if (divider != null) {
                divider.setVisibility(showDivider ? View.VISIBLE : View.INVISIBLE);
            }
        }
    };

//    @Override
//    protected void onResume() {
//        super.onResume();
//        whiteListCheckAsyncTask = new WhiteListCheckAsyncTask(this);
//        whiteListCheckAsyncTask.execute(searchPack);
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (whiteListCheckAsyncTask != null && !whiteListCheckAsyncTask.isCancelled()) {
//            whiteListCheckAsyncTask.cancel(true);
//        }
//    }

    private void updateAddUI(Boolean isWhitelisted) {
        if (isWhitelisted) {
//            addButton.setVisibility(View.GONE);
//            alreadyAddedText.setVisibility(View.VISIBLE);
        } else {
//            addButton.setVisibility(View.VISIBLE);
//            alreadyAddedText.setVisibility(View.GONE);
        }
    }

//    static class WhiteListCheckAsyncTask extends AsyncTask<StickerSearchPack, Void, Boolean> {
//        private final WeakReference<SearchStickerPackDetailsActivity> searchStickerPackDetailsActivityWeakReference;
//
//        WhiteListCheckAsyncTask(SearchStickerPackDetailsActivity stickerPackListActivity) {
//            this.searchStickerPackDetailsActivityWeakReference= new WeakReference<>(stickerPackListActivity);
//        }
//
//        @Override
//        protected final Boolean doInBackground(StickerSearchPack... stickerPacks) {
//            StickerSearchPack stickerPack = stickerPacks[0];
//            final SearchStickerPackDetailsActivity searchStickerPackDetailsActivity = searchStickerPackDetailsActivityWeakReference.get();
//            //noinspection SimplifiableIfStatement
//            if (searchStickerPackDetailsActivity == null) {
//                return false;
//            }
//            return WhitelistCheck.isWhitelisted(searchStickerPackDetailsActivity, stickerPack.identifier);
//        }
//
//        @Override
//        protected void onPostExecute(Boolean isWhitelisted) {
//            final SearchStickerPackDetailsActivity searchStickerPackDetailsActivity = searchStickerPackDetailsActivityWeakReference.get();
//            if (searchStickerPackDetailsActivity != null) {
//                searchStickerPackDetailsActivity.updateAddUI(isWhitelisted);
//            }
//        }
//    }
public  class getBitmap2 extends AsyncTask<JSONArray, Void, String> {
    String url1 = "", stickerpath = null, id, packname = null;
    JSONObject objpack;
    JSONArray jsonArrayarr;
    StickerSearchPack pack;
    StickerPack stickerpack;
    int stickerCount = 0, pos;
    Uri uri, trayuri;


    public getBitmap2(StickerSearchPack pack, int pos, String packname, Uri uri) {
        this.pack = pack;
        this.pos = pos;
        this.packname = packname;
        this.trayuri = uri;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.show();
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
    }

    protected String doInBackground(JSONArray... urls) {

        try {
//                        context=(Context) StickerPackSearchList.this;
            URL url2 = new URL(API_URL);
            HttpURLConnection urlConnection2 = (HttpURLConnection) url2.openConnection();

            try {
//                            Log.e("current", "p" + pack.getTrayImageUri());
//                trayuri = pack.getTrayImageUri();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection2.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                String response = stringBuilder.toString();
                JSONObject objMAin = new JSONObject(response);
                JSONArray arr = objMAin.getJSONArray("packs");
                JSONObject obj = (JSONObject) arr.get(0);
                JSONArray arrpacks = obj.getJSONArray("sticker_packs");

                for (int k = 0; k < arrpacks.length(); k++) {
//
                    String tmppckname = arrpacks.getJSONObject(k).getString("identifier");
                    if (tmppckname.equalsIgnoreCase(packname)) {
                        Log.e("chk:2:", "chk:2:" + arrpacks.getJSONObject(k).getString("identifier") + "%%%" + packname);
                        JSONObject objarr = arrpacks.getJSONObject(k);

//                            JSONObject objarr = arrpacks.getJSONObject(pos);
                        JSONArray jsonArrayarr = objarr.getJSONArray("stickers");
                        String id = objarr.getString("identifier");
                        Log.e("i", "id" + id);

                        stickerpack = new StickerPack(pack.getIdentifier(), pack.getName(), pack.getPublisher(), trayuri, "", "", "", "", SearchStickerPackDetailsActivity.this);
//                        jsonArrayarr = urls[0];
                         Log.e("id","o"+trayuri);
                        for (int j = 0; j < jsonArrayarr.length(); j++) {
                            String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
                             StorageReference islandRef2 = storage.getReference();

                            final StorageReference downloadRef2;
//                            downloadRef2 = storage.getReferenceFromUrl(stickers);
                            downloadRef2 = islandRef2.child(stickers);
                            try {
//                                  StorageReference islandRef2 = storage.getReferenceFromUrl("gs://smart-stickers.appspot.com").child(urlMain);
                                File rootPath = new File(Environment.getExternalStorageDirectory().getAbsoluteFile(), "smartStickers");

                                if (!rootPath.exists()) {
                                    rootPath.mkdirs();
                                }
                                final File localFile = new File(rootPath, id + stickerCount + ".png");
                                stickerCount++;

                                downloadRef2.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                        Log.e("firebase ", ";local tem file created  created " + Uri.parse(String.valueOf(localFile)));
//                                    uri= Uri.parse(String.valueOf(localFile));
                                        uri = Uri.fromFile(localFile);
                                    stickerpath = String.valueOf(localFile);
                                    Log.e("dr", "i" + uri);
//                                    setStickers(uri,stickerpath,stickerpack);
                                    lists.add(String.valueOf(localFile));

//                                    stickerpack.addStickerDownloaded(uri,pack.getIdentifier(), SearchStickerPackDetailsActivity.this);
//                                    stickerpack.getIdentifier()
                                    Log.e("lists", "apss" + lists.size() + " " + jsonArrayarr.length()+stickerpack.getTrayImageUri());
                                    if (jsonArrayarr.length() == lists.size()) {
                                        StickerBook.addStickerPackExisting(stickerpack);
                                        progressDialog.dismiss();
                                        isdownload=true;
                                        Log.e("00000000","ooo"+index);
//
//                                        if (packname != null) {
//                                            Toast.makeText(getApplicationContext(), packname + " Downloaded.", Toast.LENGTH_LONG).show();
//                                        }
                                    }
                                        //  updateDb(timestamp,localFile.toString(),position);
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        Log.e("firebase ", ";local tem file not created  created " + exception.toString());
                                    }
                                });

//                                Log.e("locall","go" + Uri.parse(String.valueOf(localFile)));
//                                uri = Uri.fromFile(localFile);
//                                    stickerpath = String.valueOf(localFile);
//                                    Log.e("dr", "i" + uri);
////                                    setStickers(uri,stickerpath,stickerpack);
//                                    lists.add(String.valueOf(localFile));
//
//                                    stickerpack.addStickerDownloaded(uri,pack.getIdentifier(), SearchStickerPackDetailsActivity.this);
////                                    stickerpack.getIdentifier()
//                                    Log.e("lists", "apss" + lists.size() + " " + jsonArrayarr.length()+stickerpack.getTrayImageUri());
//                                    if (jsonArrayarr.length() == lists.size()) {
//                                        StickerBook.addStickerPackExisting(stickerpack);
//                                        progressDialog.dismiss();
//                                        isdownload=true;
//                                        Log.e("00000000","ooo"+index);
////
////                                        if (packname != null) {
////                                            Toast.makeText(getApplicationContext(), packname + " Downloaded.", Toast.LENGTH_LONG).show();
////                                        }
//                                    }
//
                            }catch(Exception e){
                                e.printStackTrace();
                            }





//                            final File localFile = new File(rootPath, id + stickerCount + ".png");
//                            downloadRef2.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
//                                @Override
//                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
//                                    Log.e("firebase ","; created  created " +localFile.toString());
//                                    //  updateDb(timestamp,localFile.toString(),position);
//                                }
//                            }).addOnFailureListener(new OnFailureListener() {
//                                @Override
//                                public void onFailure(@NonNull Exception exception) {
//                                    Log.e("firebase ",";local tem file not " +exception.toString());
//                                }
//                            });
//                            downloadRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
//                                @Override
//                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
//                                    Log.e("firebase ", ";local tem file created  created " + Uri.parse(String.valueOf(localFile)));
////                                    uri= Uri.parse(String.valueOf(localFile));
//                                    uri = Uri.fromFile(localFile);
//                                    stickerpath = String.valueOf(localFile);
//                                    Log.e("dr", "i" + uri);
////                                    setStickers(uri,stickerpath,stickerpack);
//                                    lists.add(String.valueOf(localFile));
//
//                                    stickerpack.addSticker(uri, SearchStickerPackDetailsActivity.this);
////                                    stickerpack.getIdentifier()
//                                    Log.e("lists", "apss" + lists.size() + " " + stickerCount);
//                                    if (stickerCount == lists.size()) {
//                                        StickerBook.addStickerPackExisting(stickerpack);
//                                        progressDialog.dismiss();
//                                        if (packname != null) {
//                                            Toast.makeText(getApplicationContext(), packname + " Downloaded.", Toast.LENGTH_LONG).show();
//                                        }
//                                    }
//                                    //  updateDb(timestamp,localFile.toString(),position);
//                                }
//                            }).addOnFailureListener(new OnFailureListener() {
//                                @Override
//                                public void onFailure(@NonNull Exception exception) {
//                                    Log.e("firebase ", ";local tem file not created  created " + exception.toString());
//                                }
//                            });

//                            URL url = new URL(stickers);
//                            File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
//                            String filename = id + stickerCount + ".png";
//                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//                            urlConnection.setRequestMethod("GET");
//                            urlConnection.setDoOutput(true);
//                            urlConnection.connect();
//                            stickerCount++;
//                            Log.i("Local filename:", "" + filename + stickerCount);
//                            File file = new File(SDCardRoot, filename);
//                            if (file.createNewFile()) {
//                                file.createNewFile();
//                            }
//                            FileOutputStream fileOutput = new FileOutputStream(file);
//                            InputStream inputStream = urlConnection.getInputStream();
//                            int totalSize = urlConnection.getContentLength();
//                            int downloadedSize = 0;
//                            byte[] buffer = new byte[2048];
//                            int bufferLength = 0;
//                            long total = 0;
//                            InputStream input = new BufferedInputStream(
//                                    url.openStream(), 78000);
//                            while ((count = input.read(buffer)) != -1) {
//                                total += count;
//                                // writing data to file
//                                fileOutput.write(buffer, 0, count);
//                            }
//                            fileOutput.flush();
//                            input.close();
//
//                            fileOutput.close();
//                if (downloadedSize == totalSize)

                        }
                    }

                }
//                            Log.e("list", "os" + lists.size() + " " + stickerCount);

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                urlConnection2.disconnect();
            }
//
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return stickerpath;

    }

    @SuppressLint("RestrictedApi")
    protected void onPostExecute(String response) {
//            progressBar.setVisibility(View.INVISIBLE);
        if (response == null) {
            response = "THERE WAS AN ERROR";
        }
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(SearchStickerPackDetailsActivity.this);
            builder.setMessage("Download Successful")
                    .setCancelable(true)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int id){
                            dialog.dismiss();
//                        openFileTray(nameBox.getText().toString(), creatorBox);
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
            deleteButton.setVisibility(View.INVISIBLE);
//            deleteButton.setImageResource(R.drawable.downd);
//            deleteButton.setVisibility(v.INVISIBLE);
        Log.e("index","ikkk"+index);

//            SearchBook.init(SearchStickerPackDetailsActivity.this);
            SearchBook.updatepack(index);
//                Log.e("res", "is" + response);

//                Log.e("list", "os" + lists.size() + " " + stickerCount);
//                if (stickerCount == lists.size()) {
//                    StickerBook.addStickerPackExisting(stickerpack);
//                    progressDialog.dismiss();
//                }

//                addStickerPackExisting(pack);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

}