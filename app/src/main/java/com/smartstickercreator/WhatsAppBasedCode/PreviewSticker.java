package com.smartstickercreator.WhatsAppBasedCode;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.smartstickercreator.R;
import com.smartstickercreator.StickerBook;

public class PreviewSticker extends AppCompatActivity {
    ImageView imgView;
    public StickerPack stickerPack;
    String identifier;
    int pos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_sticker);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imgView=(ImageView)findViewById(R.id.imagePreview);
        identifier=getIntent().getStringExtra("stickerPack");
        pos= Integer.parseInt(getIntent().getStringExtra("position"));
        stickerPack= StickerBook.getStickerPackById(identifier);
        Log.e("o","i"+stickerPack);
//        Sticker thisSticker = stickerPack.getSticker(pos);
//        Log.e("pack","l"+thisSticker);
//        imgView.setImageURI(thisSticker.getUri());
//        setContentView(new CropView(PreviewSticker.this));
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if(item.getItemId() == R.id.action_info) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
//                            if(stickerPack.getStickers().size()>3 || !WhitelistCheck.isWhitelisted(this, stickerPack.getIdentifier())){
//                                dialogInterface.dismiss();
//                                stickerPack.deleteSticker(thisSticker);
////                                Activity thisActivity = this;
//                                PreviewSticker.this.finish();
////                                this.startActivity(this.getIntent());
//                                Toast.makeText(PreviewSticker.this, "Sticker deleted", Toast.LENGTH_SHORT).show();
//                            } else {
//                                AlertDialog alertDialog = new AlertDialog.Builder(PreviewSticker.this)
//                                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialogInterface, int i) {
//                                                dialogInterface.dismiss();
//                                            }
//                                        }).create();
//                                alertDialog.setTitle("Invalid Action");
//                                alertDialog.setMessage("A sticker pack that is already applied to WhatsApp cannot have less than 3 stickers. " +
//                                        "In order to remove additional stickers, please add more to the pack first or remove the pack from the WhatsApp app.");
//                                alertDialog.show();
//                            }
                        }
                    })
                    .create();
            alertDialog.setTitle("Are yoy sure Delete sticker from the Pack?");
            alertDialog.setMessage("Deleting this sticker will delete from WhatsApp Automatically.");
            alertDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
