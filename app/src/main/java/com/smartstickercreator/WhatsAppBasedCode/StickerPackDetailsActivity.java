/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.smartstickercreator.WhatsAppBasedCode;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.smartstickercreator.BuildConfig;
import com.smartstickercreator.R;
import com.smartstickercreator.StickerBook;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.Objects;

import static com.smartstickercreator.WhatsAppBasedCode.StickerPackListActivity.context;
import static com.smartstickercreator.WhatsAppBasedCode.StickerPackListActivity.getImageUri;

public class StickerPackDetailsActivity extends BaseActivity {

    /**
     * Do not change below values of below 3 lines as this is also used by WhatsApp
     */
    public static final String EXTRA_STICKER_PACK_ID = "sticker_pack_id";
    public static final String EXTRA_STICKER_PACK_AUTHORITY = "sticker_pack_authority";
    public static final String EXTRA_STICKER_PACK_NAME = "sticker_pack_name";

    public static final int ADD_PACK = 200;
    public static final String EXTRA_STICKER_PACK_WEBSITE = "sticker_pack_website";
    public static final String EXTRA_STICKER_PACK_EMAIL = "sticker_pack_email";
    public static final String EXTRA_STICKER_PACK_PRIVACY_POLICY = "sticker_pack_privacy_policy";
    public static final String EXTRA_STICKER_PACK_TRAY_ICON = "sticker_pack_tray_icon";
    public static final String EXTRA_SHOW_UP_BUTTON = "show_up_button";
    public static final String EXTRA_STICKER_PACK_DATA = "sticker_pack";
    private static final String TAG = "StickerPackDetails";

    private RecyclerView recyclerView;
    private GridLayoutManager layoutManager;
RecyclerView.LayoutManager recyclerViewLayoutManager;
    private StickerPreviewAdapter stickerPreviewAdapter;
    private int numColumns;
    private View addButton;
    private View alreadyAddedText;
    private StickerPack stickerPack;
    private View divider;
    private WhiteListCheckAsyncTask whiteListCheckAsyncTask;
    private FrameLayout shareButton,detailFrame;
//    private FrameLayout deleteButton;
    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private ImageView deleteButton,image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sticker_pack_details);

        MobileAds.initialize(this, getString(R.string.admob_ad_id));
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest
                .Builder()
                .addTestDevice(getString(R.string.test_device))
                .build();
//        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());

        boolean showUpButton = getIntent().getBooleanExtra(EXTRA_SHOW_UP_BUTTON, false);
        stickerPack = StickerBook.getStickerPackById(getIntent().getStringExtra(EXTRA_STICKER_PACK_DATA));
        TextView packNameTextView = findViewById(R.id.pack_name);
//        TextView packPublisherTextView = findViewById(R.id.author);
        ImageView packTrayIcon = findViewById(R.id.tray_image);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(StickerPackDetailsActivity.this,EditStickersActivity.class);
                in.putExtra("stickerPack",stickerPack.getIdentifier());
                startActivity(in);
//                openFile();
            }
        });
        detailFrame=findViewById(R.id.detailFrame);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        addButton =findViewById(R.id.add_to_whatsapp_button);
//        shareButton = findViewById(R.id.share_pack_button);
        deleteButton = findViewById(R.id.delimage);
        image=findViewById(R.id.image);
        alreadyAddedText = findViewById(R.id.already_added_text);
        int cp=calculateNoOfColumns(StickerPackDetailsActivity.this);
        Log.e("i","cols"+cp);
        layoutManager = new GridLayoutManager(StickerPackDetailsActivity.this,3);
        recyclerViewLayoutManager=new GridLayoutManager(StickerPackDetailsActivity.this,3);
        recyclerView = findViewById(R.id.sticker_list);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
//        layoutManager.setOrientation(0);
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(pageLayoutListener);
        recyclerView.addOnScrollListener(dividerScrollListener);
        divider = findViewById(R.id.divider);
        if (stickerPreviewAdapter == null) {
            stickerPreviewAdapter = new StickerPreviewAdapter(StickerPackDetailsActivity.this,getLayoutInflater(), R.drawable.sticker_error, 205, 0, stickerPack);
//            Log.e("pack","added"+stickerPreviewAdapter.getItemCount());
            recyclerView.setAdapter(stickerPreviewAdapter);
            stickerPreviewAdapter.notifyDataSetChanged();


        }
        if(stickerPack.getTrayImageUri()!=null) {
//            Sticker thisSticker= stickerPack.getStickerById(0);
//            ArrayList<StickerPack> arr=StickerBook.checkIfPacksAreNull();
//          List<Sticker> list=stickerPack.getStickers();
//            Log.e("stickers","k"+list.get(0));
            Uri  uri=stickerPack.getTrayImageUri();
            packTrayIcon.setImageURI(uri);
        }else{
            Log.e("bitmap","uri null");
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                    R.drawable.stickermakerlogo);
            packTrayIcon.setImageBitmap(bitmap);

        }
        packNameTextView.setText(stickerPack.name);


//        packPublisherTextView.setText(stickerPack.publisher);


//        findViewById(R.id.add_sticker_to_pack).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                openFile();
//            }
//        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(StickerPackDetailsActivity.this,StickerPackListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                intent.setType(Intent.ACTION_MAIN);
                startActivity(intent);
//           StickerPackListActivity.class));
//                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//// This uses android:parentActivityName and
//// android.support.PARENT_ACTIVITY meta-data by default
//                stackBuilder.addNextIntentWithParentStack(StickerPackListActivity);
//                PendingIntent pendingIntent = stackBuilder
//                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                StickerPackDetailsActivity.this.finish();

//                Intent intent=new Intent(StickerPackDetailsActivity.this,StickerPackListActivity.class);
//                startActivity(intent);
            }
        });
        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(stickerPack.getStickers().size()>=3) {

                    StickerPackDetailsActivity.this.addStickerPackToWhatsApp(stickerPack);
                } else {


                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                            R.drawable.logo);

                    Uri uri=getImageUri(context,bitmap);
                    stickerPack.addSticker(uri, context);

                    Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(),
                            R.drawable.heart2);

                    Uri uri2=getImageUri(context,bitmap2);
                    stickerPack.addSticker(uri2, context);
                    StickerPackDetailsActivity.this.addStickerPackToWhatsApp(stickerPack);



//                    AlertDialog alertDialog = new AlertDialog.Builder(StickerPackDetailsActivity.this)
//                            .setNegativeButton("Got It", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    dialogInterface.dismiss();
//                                }
//                            }).create();
//                    alertDialog.setTitle("Invalid Action");
//                    alertDialog.setMessage("Please add at least 3 stickers, and let's go.");
//                    alertDialog.show();
                }
            }
        });

//        shareButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DataArchiver.createZipFileFromStickerPack(stickerPack, StickerPackDetailsActivity.this);
//            }
//        });
//
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(StickerPackDetailsActivity.this)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                StickerBook.deleteStickerPackById(stickerPack.getIdentifier());
                                finish();
                                Intent intent = new Intent(StickerPackDetailsActivity.this, StickerPackListActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                Toast.makeText(StickerPackDetailsActivity.this, "Sticker collection deleted", Toast.LENGTH_SHORT).show();
                            }
                        }).create();
                alertDialog.setTitle("Are you sure to Delete sticker collection?");
                alertDialog.setMessage("Deleting this collection will also remove it from your WhatsApp app.");
                alertDialog.show();
            }
        });
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(showUpButton);
//            getSupportActionBar().setTitle(showUpButton ? R.string.title_activity_sticker_pack_details_multiple_pack : R.string.title_activity_sticker_pack_details_single_pack);
//            getSupportActionBar().setTitle(stickerPack.name);
//            setSupportActionBar(toolBar);
//        }
        Boolean name=getIntent().getBooleanExtra("add",false);
    if(name) {
        LayoutInflater layoutInflater = LayoutInflater.from(StickerPackDetailsActivity.this);
                View promptView = layoutInflater.inflate(R.layout.activity_sticker_pack_info, null);
        AlertDialog alertDialog = new AlertDialog.Builder(StickerPackDetailsActivity.this).create();
        Button btnAdd1 = (Button) promptView.findViewById(R.id.btn);
        btnAdd1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                alertDialog.dismiss();
                return false;
            }
        });
//                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                    }
//                }
//                )
//                .create();
//        alertDialog.setMessage("Please, Do not close this App just Reopen WhatsApp to see all sticker inside this collection.");
//
        alertDialog.setView(promptView);
        alertDialog.show();
    }
    }
    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }
    private void launchInfoActivity(String publisherWebsite, String publisherEmail, String privacyPolicyWebsite, String trayIconUriString) {
        Intent intent = new Intent(StickerPackDetailsActivity.this, StickerPackInfoActivity.class);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_ID, stickerPack.identifier);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_WEBSITE, publisherWebsite);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_EMAIL, publisherEmail);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_PRIVACY_POLICY, privacyPolicyWebsite);
        intent.putExtra(StickerPackDetailsActivity.EXTRA_STICKER_PACK_TRAY_ICON, stickerPack.getTrayImageUri().toString());
        startActivity(intent);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void openFile() {
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        i.setType("image/*");
        startActivityForResult(i, 3000);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBackPressed(){
        super.onBackPressed();
          Intent intent = new Intent(StickerPackDetailsActivity.this,StickerPackListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.toolbar, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.action_info && stickerPack != null) {
//            AlertDialog alertDialog = new AlertDialog.Builder(StickerPackDetailsActivity.this)
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                            }
//                        })
//                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                                StickerBook.deleteStickerPackById(stickerPack.getIdentifier());
//                                finish();
//                                Intent intent = new Intent(StickerPackDetailsActivity.this, StickerPackListActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                Toast.makeText(StickerPackDetailsActivity.this, "Sticker Pack deleted", Toast.LENGTH_SHORT).show();
//                            }
//                        }).create();
//                alertDialog.setTitle("Are you sure?");
//                alertDialog.setMessage("Deleting this package will also remove it from your WhatsApp app.");
//                alertDialog.show();
////
////            final String publisherWebsite = stickerPack.publisherWebsite;
////            final String publisherEmail = stickerPack.publisherEmail;
////            final String privacyPolicyWebsite = stickerPack.privacyPolicyWebsite;
////            Uri trayIconUri = stickerPack.getTrayImageUri();
////            launchInfoActivity(publisherWebsite, publisherEmail, privacyPolicyWebsite, trayIconUri.toString());
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    private void addStickerPackToWhatsApp(StickerPack sp) {
        Intent intent = new Intent();
        intent.setAction("com.whatsapp.intent.action.ENABLE_STICKER_PACK");
        Log.w("IS IT A NEW IDENTIFIER?", sp.getIdentifier());
        intent.putExtra(EXTRA_STICKER_PACK_ID, sp.getIdentifier());
        intent.putExtra(EXTRA_STICKER_PACK_AUTHORITY, BuildConfig.CONTENT_PROVIDER_AUTHORITY);
        intent.putExtra(EXTRA_STICKER_PACK_NAME, sp.getName());
        try {
            startActivityForResult(intent, 200);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, R.string.error_adding_sticker_pack, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_PACK) {
            if (resultCode == Activity.RESULT_CANCELED && data != null) {
                final String validationError = data.getStringExtra("validation_error");
                if (validationError != null) {
                    if (BuildConfig.DEBUG) {
                        //validation error should be shown to developer only, not users.
                        MessageDialogFragment.newInstance(R.string.title_validation_error, validationError).show(getSupportFragmentManager(), "validation error");
                    }
                    Log.e(TAG, "Validation failed:" + validationError);
                }
            } else {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    mInterstitialAd = new InterstitialAd(this);
                    mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
                    mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());
                }
            }
        } else if(requestCode == 3000){
            if(data!=null){
                if(data.getClipData()!=null){
                    ClipData clipData = data.getClipData();
                    for(int i = 0; i < clipData.getItemCount(); i++)
                    {
                        ClipData.Item path = clipData.getItemAt(i);
                        Uri uri = path.getUri();
                        getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                        setImg(uri);
//                        setContentView(new CropView(PreviewSticker.this,bmp));
//
//
//
// Intent in=new Intent(StickerPackDetailsActivity.this,EditStickersActivity.class);
////                in.putExtra("im",uri);
//                startActivity(in);
                                                stickerPack.addSticker(uri, this);
                    }
                } else {
                    Uri uri = data.getData();
                    getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                    Intent in=new Intent(StickerPackDetailsActivity.this,EditStickersActivity.class);
////                    in.putExtra("im",uri);
//                    startActivity(in);
                                        stickerPack.addSticker(uri, this);
//                    setImg(uri);
//                    setContentView(new CropView(PreviewSticker.this,bmp));
                }
                finish();
                startActivity(getIntent());
            }
        }
    }

    private final ViewTreeObserver.OnGlobalLayoutListener pageLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            setNumColumns(recyclerView.getWidth() / recyclerView.getContext().getResources().getDimensionPixelSize(R.dimen.sticker_pack_details_image_size));
        }
    };

    private void setNumColumns(int numColumns) {
        if (this.numColumns != numColumns) {
            layoutManager.setSpanCount(numColumns);
            this.numColumns = numColumns;
            if (stickerPreviewAdapter != null) {
                stickerPreviewAdapter.notifyDataSetChanged();
            }
        }
    }

    private final RecyclerView.OnScrollListener dividerScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull final RecyclerView recyclerView, final int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            updateDivider(recyclerView);
        }

        @Override
        public void onScrolled(@NonNull final RecyclerView recyclerView, final int dx, final int dy) {
            super.onScrolled(recyclerView, dx, dy);
            updateDivider(recyclerView);
        }

        private void updateDivider(RecyclerView recyclerView) {
            boolean showDivider = recyclerView.computeVerticalScrollOffset() > 0;
            if (divider != null) {
                divider.setVisibility(showDivider ? View.VISIBLE : View.INVISIBLE);
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        whiteListCheckAsyncTask = new WhiteListCheckAsyncTask(this);
        whiteListCheckAsyncTask.execute(stickerPack);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (whiteListCheckAsyncTask != null && !whiteListCheckAsyncTask.isCancelled()) {
            whiteListCheckAsyncTask.cancel(true);
        }
    }

    private void updateAddUI(Boolean isWhitelisted) {
        if (isWhitelisted) {
            addButton.setVisibility(View.GONE);
            alreadyAddedText.setVisibility(View.VISIBLE);
        } else {
            addButton.setVisibility(View.VISIBLE);
            alreadyAddedText.setVisibility(View.GONE);
        }
    }

    static class WhiteListCheckAsyncTask extends AsyncTask<StickerPack, Void, Boolean> {
        private final WeakReference<StickerPackDetailsActivity> stickerPackDetailsActivityWeakReference;

        WhiteListCheckAsyncTask(StickerPackDetailsActivity stickerPackListActivity) {
            this.stickerPackDetailsActivityWeakReference = new WeakReference<>(stickerPackListActivity);
        }

        @Override
        protected final Boolean doInBackground(StickerPack... stickerPacks) {
            StickerPack stickerPack = stickerPacks[0];
            final StickerPackDetailsActivity stickerPackDetailsActivity = stickerPackDetailsActivityWeakReference.get();
            //noinspection SimplifiableIfStatement
            if (stickerPackDetailsActivity == null) {
                return false;
            }
            return WhitelistCheck.isWhitelisted(stickerPackDetailsActivity, stickerPack.identifier);
        }

        @Override
        protected void onPostExecute(Boolean isWhitelisted) {
            final StickerPackDetailsActivity stickerPackDetailsActivity = stickerPackDetailsActivityWeakReference.get();
            if (stickerPackDetailsActivity != null) {
                stickerPackDetailsActivity.updateAddUI(isWhitelisted);
            }
        }
    }
}