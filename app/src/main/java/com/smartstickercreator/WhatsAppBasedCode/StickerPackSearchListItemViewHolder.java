package com.smartstickercreator.WhatsAppBasedCode;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartstickercreator.R;

public class StickerPackSearchListItemViewHolder extends RecyclerView.ViewHolder {

    View container;
    TextView titleView;
    TextView publisherView;
    //TextView filesizeView;
//    ImageView addButton;
    ImageView shareButton;
    LinearLayout imageRowView;

    StickerPackSearchListItemViewHolder(final View itemView) {
        super(itemView);
        container = itemView;
        titleView = itemView.findViewById(R.id.search_pack_title);
        publisherView = itemView.findViewById(R.id.search_pack_publisher);
        //filesizeView = itemView.findViewById(R.id.sticker_pack_filesize);
//        addButton = itemView.findViewById(R.id.search_button_on_list);
//        shareButton = itemView.findViewById(R.id.export_button_on_list);
        imageRowView = itemView.findViewById(R.id.search_packs_list_item_image_list);
    }
}