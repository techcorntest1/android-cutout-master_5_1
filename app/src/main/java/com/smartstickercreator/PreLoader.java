package com.smartstickercreator;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.smartstickercreator.WhatsAppBasedCode.StickerPack;
import com.smartstickercreator.WhatsAppBasedCode.StickerPackListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.smartstickercreator.NewUserIntroActivity.verifyStoragePermissions;

public class PreLoader extends AppCompatActivity {
    public static String API_URL="http://erronak.com/dev/smartstickers/";
    SharedPreferences.Editor mEditor;
    int count=0,stickerCount=0;
    String versioncode = "", sharedVal,path;
    ImageView img;
    List<StickerPack> list=new ArrayList<StickerPack>();
    SharedPreferences prefs;
    String StorezipFileLocation="http://erronak.com/dev/smartstickers/assets/Stickerpack.zip";
    private ProgressDialog mProgressDialog;
    String DirectoryName=Environment.getExternalStorageDirectory() + "/downloads/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_loader);
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        int permission = ActivityCompat.checkSelfPermission(PreLoader.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED){
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    PreLoader.this,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    1
            );
        } else{
            Log.e("grant", "Do");
//            SharedPreferences mSettings = this.getSharedPreferences("StickerMaker", Context.MODE_PRIVATE);
//            mEditor = mSettings.edit();
//            sharedVal = mSettings.getString("version", "0");
//            Log.e("val", "is" + sharedVal);
//            new VersionTask().execute();
//                     SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//            Boolean show = prefs.getBoolean("isAlreadyShown", true);
//
//                    // Instantiate the RequestQueue.
                    new RetrieveFeedTask().execute();

//            new DownloadZipfile().execute(StorezipFileLocation);
//            new DownloadZipfile(Environment.getExternalStorageDirectory()+"sticker", PreLoader.this,new DownloadZipfile.PostDownload(){
//                           @Override
//                           public void downloadDone(File file) {
//                               Log.i("TAG", "file download completed"+file);
//
//                               // check unzip file now
//                               Decompress unzip = new Decompress(PreLoader.this, file);
//                               unzip.unzip();
//
//                               Log.i("TAG", "file unzip completed");
//                           }
//                       }).execute(StorezipFileLocation);
        }



        SharedPreferences mSettings = this.getSharedPreferences("StickerMaker", Context.MODE_PRIVATE);
        mEditor = mSettings.edit();
        sharedVal = mSettings.getString("version", "0");
        Log.e("val", "is" + sharedVal);

//        if (sharedVal.matches(versioncode)){
//
//        } else {
//            Log.e("not", "match" + versioncode + " " + sharedVal);
//            // Instantiate the RequestQueue.
//            new RetrieveFeedTask().execute();
//        }
//        int permission2 = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
//        if (permission2 != PackageManager.PERMISSION_GRANTED) {
//            verifyStoragePermissions(PreLoader.this);
//        }


    }

    private void makeIntroNotRunAgain() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean previouslyStarted = prefs.getBoolean("isAlreadyShown", false);
        if (!previouslyStarted) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean("isAlreadyShown", false);
            edit.commit();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == StickerPackDetailsActivity.ADD_PACK) {
//            if (resultCode == Activity.RESULT_CANCELED && data != null) {
//                final String validationError = data.getStringExtra("validation_error");
//                if (validationError != null) {
//                    if (BuildConfig.DEBUG) {
//                        //validation error should be shown to developer only, not users.
//                        BaseActivity.MessageDialogFragment.newInstance(R.string.title_validation_error, validationError).show(getSupportFragmentManager(), "validation error");
//                    }
//                    Log.e(TAG, "Validation failed:" + validationError);
//                }
//            } else {
//                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
//                    mInterstitialAd = new InterstitialAd(this);
//                    mInterstitialAd.setAdUnitId(getString(R.string.admob_fullscreen_adding_pack_unit_id));
//                    mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice(getString(R.string.test_device)).build());
//                }
//            }
//        } else if (data != null && requestCode == 2319) {
//            Bitmap mBitmap = null;
//            Uri uri = data.getData();
//            Log.e("uri", "iss" + uri);
//            getContentResolver().takePersistableUriPermission(Objects.requireNonNull(uri), Intent.FLAG_GRANT_READ_URI_PERMISSION);
//            try {
//                mBitmap = MediaStore.Images.Media.getBitmap(StickerPackListActivity.this.getContentResolver(), uri);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            Bitmap imageBitmap = getResizedBitmap(mBitmap, 96, 96);
//            Uri mUri = getImageUri(this, imageBitmap);
//            createNewStickerPackAndOpenIt(newName, newCreator, mUri);
//        } else
            if (requestCode == 1114) {
//            new RetrieveFeedTask().execute();
            makeIntroNotRunAgain();
        }
    }
    private boolean toShowIntro() {

        return prefs.getBoolean("isAlreadyShown", true);
    }
    public class VersionTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        List<StickerPack> list;

        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
//            String email = emailText.getText().toString();
            // Do some validation here

            try {
                URL url = new URL(API_URL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } catch (IOException e)
                {
                    e.printStackTrace();
                }
                finally {
                    urlConnection.disconnect();
                }

            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }

            return null;
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
//            progressBar.setVisibility(View.GONE);
//            Log.i("INFO", response);
            try {
                JSONObject objMAin=new JSONObject(response);
                versioncode=objMAin.getString("version");
                Log.e("verrrr","is"+versioncode);
                mEditor.putString("version",versioncode);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;
        private String version;
        List<StickerPack> list;

        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
//            String email = emailText.getText().toString();
            // Do some validation here

            try {
                URL url = new URL(API_URL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
//            progressBar.setVisibility(View.GONE);
//            Log.i("INFO", response);
            try {
                JSONObject objMAin=new JSONObject(response);
                version=objMAin.getString("version");
                list=new ArrayList<StickerPack>();
                Log.e("ver","is"+version);
                JSONArray arr=objMAin.getJSONArray("packs");
                JSONObject obj= (JSONObject) arr.get(0);
                JSONArray arr2 =obj.getJSONArray("sticker_packs");
//                for(int i=0;i<arr2.length();i++){
//                    JSONObject objpack=arr2.getJSONObject(i);
//                    Log.e("obj","pack"+objpack);
//                    String urlMain = objpack.getString("tray_image_file");
                new getBitmap1(arr2).execute(arr2);

//                    new getBitmap1(arr2).execute(arr2.toString());
//                }

                Log.e("listf","of"+list);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public class getBitmap1 extends AsyncTask<JSONArray, Void, String> {

        private Exception exception;
        private String version;
        String url1="",filepath=null,abs;
        JSONObject objpack;
        JSONArray arr2;
        int total,i;

        public getBitmap1(JSONArray arr2) {
//            url1=src;
//            objpack=obj;
            arr2=arr2;
//            total=total;
//            abs=abs;
//            i=i;
        }

        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(JSONArray... urls) {
            arr2 = urls[0];
            // Do some validation here
//            arr2=urls[0];
            Log.e("arr","is" +arr2);
            try {

                for(int i=0;i<arr2.length();i++) {

                    JSONObject  objpack = arr2.getJSONObject(i);
                    Log.e("obj", "pack" + objpack);
                    String urlMain = objpack.getString("tray_image_file");


                    URL url = new URL(urlMain);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setDoOutput(true);
                    urlConnection.connect();
                    File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
                    String filename = "downloadedTray" + count + ".png";
                    count++;
                    Log.i("Local filename:", "" + filename + count);
                    File file = new File(SDCardRoot, filename);
                    if (file.createNewFile()) {
                        file.createNewFile();
                    }
                    FileOutputStream fileOutput = new FileOutputStream(file);
                    InputStream inputStream = urlConnection.getInputStream();
                    int totalSize = urlConnection.getContentLength();
                    int downloadedSize = 0;
                    byte[] buffer = new byte[1024];
                    int bufferLength = 0;
                    while ((bufferLength = inputStream.read(buffer)) > 0) {
                        fileOutput.write(buffer, 0, bufferLength);
                        downloadedSize += bufferLength;
                        Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);
                    }
                    fileOutput.close();
                    if (downloadedSize == totalSize)
                        filepath = file.getPath();

                    Log.i("filepath:"," "+filepath);
                    Log.e("im","j"+Uri.fromFile(new File(filepath)) +"**pk       "+objpack);
                    StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), Uri.fromFile(new File(filepath)), "", "", "", "", PreLoader.this);
                    JSONArray jsonArrayarr = objpack.getJSONArray("stickers");
                    Log.e("s", "p" + stickerPack);

//                    new UnZipTask().execute(StorezipFileLocation, DirectoryName);
//                    new getBitmap2(stickerPack).execute(jsonArrayarr);



//              StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), contentUri, "", "", "", "", StickerPackListActivity.this);
//                    for (int j = 0; j < jsonArrayarr.length(); j++) {
//                        String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
////                    File folder = new File(stickers);
////                    new getBitmap2(stickers,stickerPack).execute();
////                           Uri stickUri =Uri.parse(path);
////                            Log.e("S", "p" + Uri.fromFile(folder));
//                    }
                    list.add(stickerPack);

//                    addStickerPackExisting(stickerPack);
                    passIntent(stickerPack,count);

                }
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                filepath=null;
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            Log.i("filepath:"," "+filepath);
                return null;
//             catch (IOException e) {
//                // Log exception
//                return null;
//            }
        }
        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            try {
//                Log.e("im","j"+Uri.fromFile(new File(response)) +"**pk       "+objpack);
//                StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), Uri.fromFile(new File(response)), "", "", "", "", PreLoader.this);
//                JSONArray jsonArrayarr = objpack.getJSONArray("stickers");
//                Log.e("s", "p" + stickerPack);
////              StickerPack stickerPack = new StickerPack(objpack.getString("identifier"), objpack.getString("name"), objpack.getString("publisher"), contentUri, "", "", "", "", StickerPackListActivity.this);
//                for (int j = 0; j < jsonArrayarr.length(); j++) {
//                    String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");
////                    File folder = new File(stickers);
////                    new getBitmap2(stickers,stickerPack).execute();
////                           Uri stickUri =Uri.parse(path);
////                            Log.e("S", "p" + Uri.fromFile(folder));
//                }
//                list.add(stickerPack);
//                addStickerPackExisting(stickerPack);
//                passIntent(stickerPack,count);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }
    public void passIntent(StickerPack sp,int total){
        Log.e("list","is"+list.size() +total +toShowIntro());
//        DataArchiver.writeStickerBookJSON(list, PreLoader.this);
        if(list.size()==total) {
            Log.e("tot","p"+(total) +list.size());
            DataArchiver.writeStickerBookJSON(list, PreLoader.this);
//            StickerBook.init(PreLoader.this);
            Intent intent = new Intent(PreLoader.this, StickerPackListActivity.class);
//            intent.putParcelableArrayListExtra("listPack", (ArrayList<StickerPack>) list);
            startActivity(intent);
        }
    }

    public class getBitmap2 extends AsyncTask<JSONArray, Void, String> {
        String url1="",stickerpath=null;
        JSONObject objpack;
        JSONArray jsonArrayarr;
        StickerPack pack;

        public getBitmap2(StickerPack pack) {
//            url1=src;
            pack=pack;
        }

        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(JSONArray... urls) {
            try {
                jsonArrayarr=urls[0];
                for (int j = 0; j < jsonArrayarr.length(); j++) {
                    String stickers = jsonArrayarr.getJSONObject(j).getString("image_file");

                URL url = new URL(stickers);
                File SDCardRoot = Environment.getExternalStorageDirectory().getAbsoluteFile();
                String filename = "downloadedSticker" + stickerCount + ".png";
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                    stickerCount++;
                Log.i("Local filename:", "" + filename + stickerCount);
                File file = new File(SDCardRoot, filename);
                if (file.createNewFile()) {
                    file.createNewFile();
                }
                FileOutputStream fileOutput = new FileOutputStream(file);
                InputStream inputStream = urlConnection.getInputStream();
                int totalSize = urlConnection.getContentLength();
                int downloadedSize = 0;
                byte[] buffer = new byte[2048];
                int bufferLength = 0;
                    long total = 0;
                    InputStream input = new BufferedInputStream(
                            url.openStream(), 78000);
                    while ((count = input.read(buffer)) != -1) {
                        total += count;
                        // publishing the progress....
                        // After this onProgressUpdate will be called
//                        publishProgress((int) ((total * 100)/totalSize));

                        // writing data to file
                        fileOutput.write(buffer, 0, count);
                    }
                    fileOutput.flush();
                    input.close();
//                while ((bufferLength = inputStream.read(buffer)) > 0) {
//                    fileOutput.write(buffer, 0, bufferLength);
//                    downloadedSize += bufferLength;
//                    Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);
//                }
                fileOutput.close();
//                if (downloadedSize == totalSize)
                    stickerpath = file.getPath();
                Log.i("filepath:"," "+stickerpath);

                    pack.addSticker(Uri.fromFile(new File(stickerpath)),PreLoader.this);
                }

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                stickerpath=null;
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return stickerpath;

        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            try {
                Log.e("res","is"+response);
//                list.add(pack);
//                addStickerPackExisting(pack);
//                passIntent(pack,count);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                Log.e("grant","res"+grantResults);
                if(grantResults.length>0) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        AlertDialog alertDialog = new AlertDialog.Builder(this)
                                .setPositiveButton("Let's Go", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        verifyStoragePermissions(PreLoader.this);
                                    }
                                })
                                .create();
                        alertDialog.setTitle("Notice!");
                        alertDialog.setMessage("Allowing storage permissions is crucial for the app to work. Please grant the permissions.");
                        alertDialog.show();
                    } else {
                        if (toShowIntro()){
                            startActivityForResult(new Intent(this, NewUserIntroActivity.class), 1114);

                        }

//                        new DownloadZipfile().execute(StorezipFileLocation);
                        Log.e("granted", "Done");
                        SharedPreferences mSettings = this.getSharedPreferences("StickerMaker", Context.MODE_PRIVATE);
                        mEditor = mSettings.edit();
                        sharedVal = mSettings.getString("version", "0");
                        Log.e("val", "is" + sharedVal);
                        new VersionTask().execute();
//                     SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                        Boolean show = prefs.getBoolean("isAlreadyShown", true);
                        Log.e("show", "f" + show);
//                                if(!show) {
//
//                                    if (sharedVal.matches(versioncode)){
//                                        new RetrieveFeedTask().execute();
//
//                                    } else {
//                                        Log.e("not", "match" + versioncode + " " + sharedVal);
                                        // Instantiate the RequestQueue.
                                        new RetrieveFeedTask().execute();
//                                    }
//                                }
                    }
                }
                break;
        }
    }

   public class DownloadZipfile extends AsyncTask<String, String, String>
    {
        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(PreLoader.this);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;

            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                //External directory path to save file
                folder = DirectoryName;
                Log.e("fileName","is"+fileName +" "+folder);
                //Create androiddeft folder if it does not exist
                File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                // Output stream to write file
                OutputStream output = new FileOutputStream(folder + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    Log.d("TAG", "Progress: " + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + folder + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
//
            return "Something went wrong";
        }
//

        /**
         * Updating progress bar
//         */
//        protected void onProgressUpdate(String... progress) {
//            // setting progress percentage
//            progressDialog.setProgress(Integer.parseInt(progress[0]));
//        }


        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();
//            try {
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            // Display File path after downloading
            Toast.makeText(PreLoader.this,
                    message, Toast.LENGTH_LONG).show();
        }
    }
}
