package com.smartstickercreator;


import android.content.Context;
import android.util.Log;

import com.smartstickercreator.WhatsAppBasedCode.StickerSearchPack;

import java.io.File;
import java.util.ArrayList;

import static com.smartstickercreator.WhatsAppBasedCode.StickerPackListActivity.context;

public class SearchBook {

    static Context myContext;
    public static ArrayList<StickerSearchPack> allStickerSearchPacks = checkIfPacksAreNull();

    public static void init(Context context){
        myContext = context;
        ArrayList<StickerSearchPack> lsp = DataArchiver.readsearchPackJSON(context);
        Log.e("ii","is"+lsp);
        if(lsp!=null && lsp.size()!=0){
            allStickerSearchPacks = lsp;
        }
    }
    public static void updatepack(int pos){
        allStickerSearchPacks.get(pos).setIsDownlonload(true);
       Boolean val= DataArchiver.writesearchBookJSON(allStickerSearchPacks,context);
        Log.e("ii","is"+allStickerSearchPacks +val);

    }
    public static void downloadpack(int pos,Boolean val2){
        allStickerSearchPacks.get(pos).setIsDownlonload(val2);
        Boolean val= DataArchiver.writesearchBookJSON(allStickerSearchPacks,context);
        Log.e("ii","is"+allStickerSearchPacks);

    }
    public static ArrayList<StickerSearchPack> checkIfPacksAreNull(){
        if(allStickerSearchPacks==null){
            Log.w("IS PACKS NULL?", "YES");
            return new ArrayList<>();
        }
        Log.w("IS PACKS NULL?", "NO");
        return allStickerSearchPacks;
    }

    /*public static String addNewStickerPack(String name, String publisher, String trayImage){
        String newId = UUID.randomUUID().toString();
        StickerPack sp = new StickerPack(newId,
                name,
                publisher,
                trayImage,
                "",
                "",
                "",
                "");
        allStickerPacks.add(sp);
        return newId;
    }*/

    public static void addStickerPackExisting(StickerSearchPack sp){
        allStickerSearchPacks.add(sp);
    }

    public static ArrayList<StickerSearchPack> getAllStickerPacks(){
        return allStickerSearchPacks;
    }

    public static StickerSearchPack getStickerPackByName(String stickerPackName){
        for (StickerSearchPack sp : allStickerSearchPacks){
            if(sp.getName().equals(stickerPackName)){
                return sp;
            }
        }
        return null;
    }

    public static StickerSearchPack getStickerPackById(String stickerPackId){
        if(allStickerSearchPacks.isEmpty()){
            init(myContext);
        }
        Log.w("THIS IS THE ALL STICKER", allStickerSearchPacks.toString());
        for (StickerSearchPack sp : allStickerSearchPacks){
            if(sp.getIdentifier().equals(stickerPackId)){
                return sp;
            }
        }
        return null;
    }
    public static int getStickerPackByPos(String stickerPackId){
        if(allStickerSearchPacks.isEmpty()){
            init(myContext);
        }
        Log.w("THIS IS THE ALL STICKER", allStickerSearchPacks.toString());
        for (int i=0 ;i<allStickerSearchPacks.size();i++){
            if(allStickerSearchPacks.get(i).getIdentifier().contentEquals(stickerPackId)){
                return i;
            }
        }
        return 0;
    }
    public static StickerSearchPack getStickerPackByIdWithContext(String stickerPackId, Context context){
        if(allStickerSearchPacks.isEmpty()){
            init(context);
        }
        Log.w("THIS IS THE ALL STICKER", allStickerSearchPacks.toString());
        for (StickerSearchPack sp : allStickerSearchPacks){
            if(sp.getIdentifier().equals(stickerPackId)){
                return sp;
            }
        }
        return null;
    }

    public static void deleteStickerPackById(String stickerPackId){
        StickerSearchPack myStickerPack = getStickerPackById(stickerPackId);
        new File(myStickerPack.getTrayImageUri().getPath()).getParentFile().delete();
        allStickerSearchPacks.remove(myStickerPack);
    }

    public static StickerSearchPack getStickerPackByIndex(int index){
        return allStickerSearchPacks.get(index);
    }
}
