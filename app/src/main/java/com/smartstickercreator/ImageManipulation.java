package com.smartstickercreator;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class ImageManipulation {

    public static Uri convertImageToWebP(Uri uri, String StickerBookId, String StickerId, Context context){

        try {
            Log.w("C: ", "path: " + uri);
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(Objects.requireNonNull(context).getContentResolver(),uri);

            dirChecker(context.getFilesDir()+"/"+StickerBookId);

            String path = context.getFilesDir()+"/"+StickerBookId+"/"+StickerBookId+"-"+StickerId+".webp";

            Log.w("Conversion Data: ", "path: " + path);


//            Bitmap bitmap;
//            File bitmapFile = new File(Environment.getExternalStorageDirectory() + "/" +"IMG_20181206_110550.jpg");
//            bitmap = BitmapFactory.decodeFile(String.valueOf(bitmapFile));
////
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//            Log.e("im","j"+bitmap);
//            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "img", null);
//            Log.e("fg","d"+Uri.parse(path));

//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(),uri);
//            bitmap.compress(Bitmap.CompressFormat.WEBP, 100, bytes);
//            Log.e("im","j"+bitmap);
//            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "img", null);
//            Log.e("fg","d"+Uri.parse(path));

//            OutputStream output;

            // Retrieve the image from the res folder


            // Find the SD Card path
//            File filepath = Environment.getExternalStorageDirectory();
//
//            // Create a new folder in SD Card
//            File dir = new File(filepath.getAbsolutePath()
//                    + "/Save Image Tutorial/");
//            dir.mkdirs();
//
//            // Create a name for the saved image
//            File file = new File(dir, "myimage.png");
//
//            // Show a toast message on successful save
//            Toast.makeText(context, "Image Saved to SD Card",
//                    Toast.LENGTH_SHORT).show();
//            try {
//
//                output = new FileOutputStream(path);
//
//                // Compress into png format image from 0% - 100%
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//                output.flush();
//                output.close();
//            }
//
//            catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }

//            FileOutputStream out = new FileOutputStream(path);
//            bitmap = Bitmap.createScaledBitmap(bitmap, 512, 512, true);
//
//            Log.w("IMAGE beforecomperssion", ""+FilesUtils.getUriSize(Uri.fromFile(new File(path)), context));
//
//            bitmap.compress(Bitmap.CompressFormat.WEBP, 100, out); //100-best quality
//
//            Log.w("IMAGE SIZE first", ""+Uri.parse(path));
//            String path2 = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "imgsticker", null);
//            Log.e("fg","d"+path2);
            makeSmallestBitmapCompatible(path, bitmap);
//            Bitmap bitmap = BitmapFactory.decodeFile(pathToPicture);
            return Uri.fromFile(new File(path));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uri;
    }

    public static Uri convertIconTrayToWebP(Uri uri, String StickerBookId, String StickerId, Context context){
        try {

            Bitmap bitmap = MediaStore.Images.Media.getBitmap(Objects.requireNonNull(context).getContentResolver(),uri);

            dirChecker(context.getFilesDir()+"/"+StickerBookId);

            String path = context.getFilesDir()+"/"+StickerBookId+"/"+StickerBookId+"-"+StickerId+".webp";

            Log.w("Conversion Data: ", "path:4  : " + path +uri);

            FileOutputStream out = new FileOutputStream(path);
            bitmap = Bitmap.createScaledBitmap(bitmap, 256, 256, true);
            bitmap.compress(Bitmap.CompressFormat.WEBP, 100, out); //100-best quality
            out.close();
//            OutputStream output;
//            try {
//
//                output = new FileOutputStream(path);
//
//                // Compress into png format image from 0% - 100%
//                bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
//                output.flush();
//                output.close();
//            }
//
//            catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
            return Uri.fromFile(new File(path));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uri;
    }

    private static void dirChecker(String dir) {
        File f = new File(dir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }

    private static byte[] getByteArray(Bitmap bitmap, int quality) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.WEBP,
                quality,
                bos);

        return bos.toByteArray();
    }

    private static void makeSmallestBitmapCompatible(String path, Bitmap bitmap){
        int quality = 100;
        FileOutputStream outs = null;
        try {
            outs = new FileOutputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bitmap = Bitmap.createScaledBitmap(bitmap, 512, 512, true);

        int byteArrayLength = 100000;
        ByteArrayOutputStream bos = null;

        while((byteArrayLength/1000)>=100){
            bos = new ByteArrayOutputStream();

            bitmap.compress(Bitmap.CompressFormat.WEBP,
                    quality,
                    bos);

            byteArrayLength = bos.toByteArray().length;
            quality-=10;

            Log.w("IMAGE SIZE IS NOW", byteArrayLength+"");
        }
        try {
            outs.write(bos.toByteArray());
            outs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
